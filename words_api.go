package main

import (
	"azla_go_learning/internal/char"
	"azla_go_learning/internal/cmd"
	"azla_go_learning/internal/words/aze"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var azlaConfig = cmd.GetFlag()

func (s *APIServer) handleUpdateResult(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	return s.handleUpdateResults(w, r, account.SessionID)

}

func (s *APIServer) handleUpdateResultBySessionID(w http.ResponseWriter, r *http.Request) error {
	sessionId, err := getSession(r)
	if err != nil {
		return err
	}

	return s.handleUpdateResults(w, r, sessionId)
}

func (s *APIServer) handleUpdateResults(w http.ResponseWriter, r *http.Request, sessionId int) error {
	correctOrNot := mux.Vars(r)["string"]
	question, err := getQuestionNumber(r)

	if err != nil {
		return err
	}

	session, err := s.store.GetSessionByID(sessionId)

	switch correctOrNot {
	case "correct":
		session.Sentences[question]["success"] = 1
	case "incorrect":
		session.Sentences[question]["success"] = 0
	}

	sentences := &GenerateSentence{
		Sentences: session.Sentences,
	}

	s.store.UpdateSessionSentences(sessionId, sentences)

	return WriteJSON(w, http.StatusOK, sentences.Sentences[question])

}


func (s *APIServer) handleGenerateSession(w http.ResponseWriter, r *http.Request) error {
	language := mux.Vars(r)["lang"]
	languages := new(Languages)

	switch language {
	case "aze":
		return languages.AzeSessionNormal(r, w, s)
	case "turkish":
		return languages.AzeSessionNormal(r, w, s)
	default:
		return WriteJSON(w, http.StatusNotFound, "language not supported")
	}


}

func (s *APIServer) handleGenerateReviewSession(w http.ResponseWriter, r *http.Request) error {
	language := mux.Vars(r)["lang"]
	languages := new(Languages)

	switch language {
	case "aze":
		return languages.AzeSessionReview(r, w, s)
	default:
		return WriteJSON(w, http.StatusNotFound, "language not supported")
	}

}


func (s *APIServer) handleGenerateSkipSession(w http.ResponseWriter, r *http.Request) error {
	language := mux.Vars(r)["lang"]
	languages := new(Languages)

	switch language {
	case "aze":
		return languages.AzeSessionSkip(r, w, s)
	default:
		return WriteJSON(w, http.StatusNotFound, "language not supported")
	}
}

func (s *APIServer) handleGetQuestion(w http.ResponseWriter, r *http.Request, sessionId int) error {
	question, err := getQuestionNumber(r)
	if err != nil {
		return err
	}

	session, err := s.store.GetSessionByID(sessionId)

	if session.IsCompleted {
		return fmt.Errorf("session %d is completed", sessionId)
	}

	if len(session.Sentences[question]) == 0 {
		return WriteJSON(w, http.StatusOK, map[string]bool{"noWords": true})
	}

	return WriteJSON(w, http.StatusOK, session.Sentences[question])
}

func (s *APIServer) handleGetQuestionByCurrent(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	return s.handleGetQuestion(w, r, account.SessionID)
}

func (s *APIServer) handleGetQuestionBySessionID(w http.ResponseWriter, r *http.Request) error {
	sessionID, err := getSession(r)
	if err != nil {
		return err
	}

	return s.handleGetQuestion(w, r, sessionID)
}

func (s *APIServer) handleEvaluateQuestionBySessionID(w http.ResponseWriter, r *http.Request) error {
	answerReq := new(EvaluateAnswerRequest)
	if err := json.NewDecoder(r.Body).Decode(&answerReq); err != nil {
		return err
	}

	sessionId, err := getSession(r)
	if err != nil {
		return nil
	}

	question, err := getQuestionNumber(r)
	if err != nil {
		return err
	}

	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	lastQuestion := s.handleNextWordBySessionID(w, r)

	session, err := s.store.GetSession(sessionId)
	if err != nil {
		return err
	}

	session.Sentences[question]["userAnswer"] = answerReq.Answer
	sentences := &GenerateSentence{
		Sentences: session.Sentences,
	}

	err = s.store.UpdateSessionSentences(sessionId, sentences)
	if err != nil {
		return err
	}

	for key, value := range session.Sentences[question] {
		switch key {
		case "correct":
			evaluateAnswers := EvaluateAnswer{
				Answer: answerReq.Answer,
				Correct: fmt.Sprintf("%v", value),
				LastQuestion: lastQuestion,
				Session: session,
				SessionId: sessionId,
				Account: account,
			}

			return s.EvaluateAnswersMain(
				w, r, evaluateAnswers)
		}

	}

	return WriteJSON(w, http.StatusOK, map[string]bool{"health": true})
}

func (s *APIServer) handleEvaluateQuestion(w http.ResponseWriter, r *http.Request) error {
	answerReq := new(EvaluateAnswerRequest)
	if err := json.NewDecoder(r.Body).Decode(&answerReq); err != nil {
		return err
	}

	question, err := getQuestionNumber(r)
	if err != nil {
		return err
	}

	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	lastQuestion := s.handleNextWord(w, r)

	session, err := s.store.GetSession(account.SessionID)
	if err != nil {
		return err
	}

	for key, value := range session.Sentences[question] {
		switch key {
		case "correct":
			evaluateAnswers := EvaluateAnswer{
				Answer: answerReq.Answer,
				Correct: fmt.Sprintf("%v", value),
				LastQuestion: lastQuestion,
				Session: session,
				SessionId: account.SessionID,
				Account: account,
			}

			return s.EvaluateAnswersMain(
				w, r, evaluateAnswers)
		}

	}

	return WriteJSON(w, http.StatusOK, map[string]bool{"health": true})
}

func (s *APIServer) UpdateResultToSession(sessionId int) error {
	correct, incorrect, _ := s.calculateCorrectAnswers(sessionId)
	err := s.store.UpdateSessionResult(sessionId,correct, "correct")
	if err != nil {
		return err
	}

	err = s.store.UpdateSessionResult(sessionId,incorrect, "incorrect")
	if err != nil {
		return err
	}

	return nil
}

// check if you have unlocked the stage, lvl or lesson
func checkForUnlocked(s *APIServer, r *http.Request, lesson *NewSessionRequest, cat, level string) error {
	account, err := s.getAccountByID(r)
	lang := "azerbajani"
	if err != nil {
		return err
	}

	progress, err := s.store.GetProgressForAccount(account.ID, lang)
	progressMap, _ := progress.Level["progress"].(map[string]interface{})

	choosenLevel := strings.Split(level, "")
	choosenLevelNum := 0
	var lastUnlockedLevel int

	for _, current := range choosenLevel {
		choosenLevelNum, _ = strconv.Atoi(current)
	}

	if cat == "Unit1" && level == "Level1" {
		return nil
	}

	if val, ok := progressMap[cat].(map[string]interface{}); ok {
		for key := range val {
			num, _ := strconv.Atoi(key)

			if num > lastUnlockedLevel {
				lastUnlockedLevel, _ = strconv.Atoi(key)
			}
		}
		if les, ok := val[strconv.Itoa(choosenLevelNum)].(map[string]interface{}); ok {
			if les[lesson.Lesson] != true {
				return fmt.Errorf("You havent unlocked this lesson yet")
			}
		}
	} else {
		return fmt.Errorf("You havent unlocked this stage yet")
	}

	if choosenLevelNum > lastUnlockedLevel {
		return fmt.Errorf("You havent unlocked this stage yet")
	}

	return nil
}

// check if you reach last question or move to the next question
func (s *APIServer) handleNextWord(_ http.ResponseWriter, r *http.Request) bool {
	account, err := s.getAccountByID(r)
	if err != nil {
		return false
	}

	session, err := s.store.GetSession(account.SessionID)
	if err != nil {
		return false
	}

	sessionMax, err := strconv.Atoi(session.MaxAmountWords)
	if err != nil {
		return false
	}

	current := session.CurrentQuestion + 1
	if session.CurrentQuestion <= sessionMax {
		err = s.store.UpdateSessionQuestion(current, account.SessionID)

		if err != nil {
			return false
		}
	}

	if session.CurrentQuestion >= sessionMax { // if you reached the end
		return true
	}

	return false
}

// check if you reach last question or move to the next question
func (s *APIServer) handleNextWordBySessionID(_ http.ResponseWriter, r *http.Request) bool {
	sessionId, err := getSession(r)
	if err != nil {
		return false
	}

	session, err := s.store.GetSession(sessionId)
	if err != nil {
		return false
	}

	sessionMax, err := strconv.Atoi(session.MaxAmountWords)
	if err != nil {
		return false
	}

	current := session.CurrentQuestion + 1
	if session.CurrentQuestion <= sessionMax {
		err = s.store.UpdateSessionQuestion(current, sessionId)

		if err != nil {
			return false
		}
	}

	if session.CurrentQuestion >= sessionMax { // if you reached the end
		return true
	}

	return false
}

func (s *APIServer) handleGetAllAzeLevels(w http.ResponseWriter, r *http.Request) error {
	allLevels := aze.GenerateAllLevels()

	return WriteJSON(w, http.StatusOK, allLevels)
}

func startQuiz(s *APIServer, r *http.Request, lessonReq *NewSessionRequest, sessionData *GenerateSentence, mode string) (int, error) {
	level := mux.Vars(r)["level"]
	stage := mux.Vars(r)["cat"]

	id, err := getID(r)
	if err != nil {
		return 0, err
	}

	account, err := s.getAccountByID(r)
	if err != nil {
		return 0, err
	}

	identifier, _ := s.GenerateSessionID()

	session := NewSession(
		account.Email,
		strconv.Itoa(lessonReq.NumberOfQuestions),
		mode, stage, level,
		lessonReq.Lesson,
		identifier,
		sessionData)

	err = s.store.CreateSession(session)
	if err != nil {
		return 0, err
	}

	sessionID := UpdateSessionID(identifier)

	if err := s.store.UpdateSessionID(id, sessionID); err != nil {
		return 0, err
	}

	return identifier, err
}

func GenerateWordOptions(sentence []aze.Sentence, selected aze.Sentence) []string {
	var alternatives []string
	for _, value := range sentence {
		if selected.Azerbaijani == value.Azerbaijani {
			word := strings.Split(value.Azerbaijani, " ")
			alternatives = word
		}
	}

	maxWord := 5
	count := 0
	for _, value := range sentence {
		count += 1
		if count == maxWord {
			break
		}

		if selected.Azerbaijani != value.Azerbaijani {
			words := strings.Split(value.Azerbaijani, " ")
			alternatives = appendAlternatives(words, alternatives)
		}
	}

	alternatives = randomizeList(alternatives)

	return alternatives
}

func appendAlternatives(words, alternatives []string) []string {
	for _, word := range words {
		alternatives = append(alternatives, word)
	}

	return alternatives
}

func randomizeList(list []string) []string {
	length := len(list)
	randomOrder := make([]string, length)
	copy(randomOrder, list) // Copy the original list to avoid modifying it

	rand.Seed(time.Now().UnixNano()) // Seed the random number generator

	// Fisher-Yates shuffle algorithm
	for i := length - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		randomOrder[i], randomOrder[j] = randomOrder[j], randomOrder[i]
	}

	return randomOrder
}

func generateQuestion(question string) (string, []string, []string) {
	split := strings.Split(question, " ")
	count := len(split)
	splits := make([]string, count)
	allWords := make([]string, count)

	randomNum := rand.Intn(count)

	for index, words := range split {
		if index != randomNum {
			splits = char.AppendStringAtIndex(splits, words, index)
		}
		allWords = append(allWords, words)
	}

	return split[randomNum], splits, allWords
}

func (s *APIServer) ReduceHealth(sessionId, health int, w http.ResponseWriter) error {
	var removeHealth int
	if health != 0 {
		removeHealth = health - 1 // Losing health
		err := s.store.UpdateSessionHealth(sessionId, removeHealth)
		if err != nil {
			return err
		}
	}

	if removeHealth == 0 {
		return nil
	}

	return nil

}

func (s *APIServer) IncreaseHealth(account *Account, health int, w http.ResponseWriter) error {
	var addHealth int
	if health != 0 {
		addHealth = health + 1 // Gaining one health
		err := s.store.UpdateSessionHealth(account.SessionID, addHealth)
		if err != nil {
			return err
		}
	}

	return nil

}

func removeSpecialChar(word string) string {
	word = strings.ReplaceAll(word, "?", "")
	word = strings.ReplaceAll(word, ",", "")

	return word
}

func convertStringToLowerAndNoSpecial(correct, answer string) (string, string) {
	correct = fmt.Sprintf("%v", correct)
	correct = removeSpecialChar(correct)
	correct = strings.ToLower(correct)
	answer = removeSpecialChar(answer)
	answer = strings.ToLower(answer)

	return answer, correct
}

func (s *APIServer) returnAltCorrectAnswers(answer, correct string, alternativeCorrect interface{}) bool {
	checkIfAltCorrect := false
	altCorrect := char.GenerateVariations(correct)
	altCorrect2 := char.GenerateVariations(fmt.Sprintf("%v", alternativeCorrect))

	for _, altC := range altCorrect {
		if answer == altC {
			checkIfAltCorrect = true
			break
		}
	}

	for _, altC := range altCorrect2 {
		if answer != "" || altC != "" {
			if answer == altC {
				checkIfAltCorrect = true
				break
			}
		}
	}

	return checkIfAltCorrect
}

// Evaluate your answers
func (s *APIServer) EvaluateAnswersMain(w http.ResponseWriter, r *http.Request, evaluate EvaluateAnswer ) error {
	question, err := getQuestionNumber(r)
	if err != nil {
		return err
	}

	sentences, _ := s.store.GetSessionByID(evaluate.SessionId)
	alternativeCorrect := sentences.Sentences[question]["altCorrect"]

	answer, correct := convertStringToLowerAndNoSpecial(evaluate.Correct, evaluate.Answer)

	if checkForNameWord(correct) == "{name}" {
		correct = strings.ReplaceAll(correct, "{name}", "")
	}

	checkIfAltCorrect := s.returnAltCorrectAnswers(answer, correct, alternativeCorrect)

	if correct == answer {
		if evaluate.Session.Health != 0 {
			return s.youAreCorrect(evaluate.LastQuestion, evaluate.SessionId, w, r)
		}
	} else if alternativeCorrect == answer && alternativeCorrect != "" {
		if evaluate.Session.Health != 0 {
			return s.youAreCorrect(evaluate.LastQuestion, evaluate.SessionId, w, r)
		}
	} else if checkIfAltCorrect {
		if evaluate.Session.Health != 0 {
			return s.youAreCorrect(evaluate.LastQuestion, evaluate.SessionId, w, r)
		}
	} else { // answer is wrong
		err := s.ReduceHealth(evaluate.SessionId, evaluate.Session.Health, w) // reduce your heat
		if err != nil {
			return err
		}

		return s.youAreInCorrect(evaluate.LastQuestion, evaluate.SessionId, w, r)
	}

	return nil
}

func (s *APIServer) updateProgressOnCompletion(r *http.Request, sessionId int) (*Progress, error) {
	session, err := s.store.GetSessionByID(sessionId)
	if err != nil {
		return nil, err
	}

	lang := session.Language
	switch session.Language {
	case "aze":
		lang = "azerbajani"
	}

	// get the completet (stage, level and lesson) from the sessions database
	stage, level, lesson := getCompleteStageFromSession(session)

	err = checkCategoryAndLevel(stage, level)
	if err != nil {
		return nil, err
	}
	
	account := new(Account)
	if account, err = s.getAccountByID(r); err != nil {
		return nil, err
	}

	currentProgress, err := s.store.GetProgressForAccount(account.ID, lang)

	if err != nil {
		return nil, err
	}

	newProgress := appendProgressOnSuccess(currentProgress, 
										   level, stage, lesson)
	progress := updateProgress(newProgress)

	if err = s.store.UpdateProgressForAccount(account.ID, progress); err != nil {
		return nil, err
	}

	return progress, nil
}

func (s *APIServer) youAreCorrect(lastQuestion bool, sessionId int, w http.ResponseWriter, r *http.Request) error {
	if lastQuestion == true {
		err := s.store.UpdateSessionIsComplete(sessionId, true)
		if err != nil {
			return err
		}

		skip, err := s.checkForSkipSession(sessionId)
		if err != nil {
			return err
		}
		
		if !skip {
			_, err = s.updateProgressOnCompletion(r, sessionId)
			if err != nil {
				return err
			}
		} else {
			correct, _, _ := s.calculateCorrectAnswers(sessionId)
			if correct >= 12 {
				err := s.skipSessionFinal(r, sessionId)
				if err != nil {
					return err
				}
				return WriteJSON(w, http.StatusOK, map[string]bool{"last": true, "correct": true, "passed": true})
			}
		}
		
		s.UpdateResultToSession(sessionId)
		s.UpdateResults(r, sessionId, "correct")
		return WriteJSON(w, http.StatusOK, map[string]bool{"last": true, "correct": true})
	} else {
		s.UpdateResults(r, sessionId, "correct")
		return WriteJSON(w, http.StatusOK, map[string]bool{"correct": true})
	}
}

func (s *APIServer) youAreInCorrect(lastQuestion bool, sessionId int, w http.ResponseWriter, r *http.Request) error {
	if lastQuestion == true {
		err := s.store.UpdateSessionIsComplete(sessionId, true)
		if err != nil {
			return err
		}

		skip, err := s.checkForSkipSession(sessionId)
		if err != nil {
			return err
		}
		
		if !skip {
			_, err = s.updateProgressOnCompletion(r, sessionId)
			if err != nil {
				return err
			}
		}

		s.UpdateResultToSession(sessionId)
		s.UpdateResults(r, sessionId, "incorrect")
		return WriteJSON(w, http.StatusOK, map[string]bool{"last": true, "incorrect": true})
	} else {
		s.UpdateResults(r, sessionId, "incorrect")
		return WriteJSON(w, http.StatusOK, map[string]bool{"incorrect": true})
	}
}

func(s *APIServer) checkForSkipSession(sessionId int) (bool, error) {
	session, err := s.store.GetSessionByID(sessionId)

	if err != nil {
		return false, err
	}

	skip, _ := session.Sentences[0]["skipsession"].(bool)

	if !skip {
		skip, _ = session.Sentences[0]["review"].(bool)
	}
	
	return skip, nil
}


func(s *APIServer) calculateCorrectAnswers(sessionId int) (int,int , error) {
	session, err := s.store.GetSessionByID(sessionId)
	if err != nil {
		return 0,0, err
	}

	maxAmount, _ := strconv.Atoi(session.MaxAmountWords)
	correct := 0
	incorrect := 0

	for i := 1; i <= maxAmount ; i++ {
		success, _ := session.Sentences[i]["success"].(int)
		if  success == 1 {
			correct += 1
		} else {
			incorrect += 1
		}
	}
	
	return correct,incorrect, nil
}


func (s *APIServer) UpdateResults(r *http.Request, sessionId int, correctOrNot string) error {
	question, err := getQuestionNumber(r)

	if err != nil {
		return err
	}

	session, err := s.store.GetSessionByID(sessionId)

	switch correctOrNot {
	case "correct":
		word, _ := session.Sentences[question]["question"].(string)	
		s.updateMistakeIfCorrect(r,word)
		session.Sentences[question]["success"] = 1
	case "incorrect":
		session.Sentences[question]["success"] = 0
	}

	sentences := &GenerateSentence{
		Sentences: session.Sentences,
	}

	s.store.UpdateSessionSentences(sessionId, sentences)

	return nil
}


func (s *APIServer) updateMistakeIfCorrect(r *http.Request, selectedWord string) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	allSessions, err := s.store.GetAllSessions(account.Email)
	if err != nil {
		return err
	}

	var sessionsToUpdate []*SaveSession

	for _, session := range allSessions {
		for i, sentence := range session.Sentences {
			updated := false
			if sentence["question"] == selectedWord {
				session.Sentences[i]["success"] = 1
				updated = true
			}

			if updated {
				sessionsToUpdate = append(sessionsToUpdate, session)
			}
		}

	}

	if len(sessionsToUpdate) > 0 {
		if err := s.store.UpdateSessionSentencesBatch(sessionsToUpdate); err != nil {
			return err
		}
	}

	return nil
}


// Generates a random game mode
func (s *APIServer) pickRandomMode() string {
	var number int

	for {
		number = rand.Intn(3)
		if number != 0 {
			break
		}
	}

	switch number {
	case 1:
		return "drag_drop"
	case 2:
		return "write"
	default:
		return "drag_drop"
	}
}

func checkForNameWord(wordToCheck string) string {
	words := strings.Split(wordToCheck, " ")
	var selectedWord string

	for _, w := range words {
		selectedWord = w
	}

	if selectedWord == "{name}" {
		return selectedWord
	} else {
		return ""
	}
}

func getCompleteStageFromSession(session *SaveSession) (string, string, int) {
	var level string
	var stage string
	var lesson int
	for selectedStage, lev := range session.SelectedLevel {
		stage = selectedStage
		for lvl, les := range lev {
			level = lvl
			lesInt, err := strconv.Atoi(les)
			if err != nil {
				fmt.Println(err)
			}

			lesson = lesInt
		}
	}

	return stage, level, lesson
}

func(s *APIServer) skipSessionFinal(r *http.Request, sessionId int) error {
	session, err := s.store.GetSessionByID(sessionId)
	if err != nil {
		return err
	}

	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}
	selectedStage := ""

	for stage := range session.SelectedLevel {
		selectedStage = stage
	}

	s.appendSkipProgress(account, selectedStage)

	return nil
}

func(s *APIServer) CreateSessionAlternatives(r *http.Request, sentence []aze.Sentence, sentences *GenerateSentence, i int, word aze.Sentence) *GenerateSentence {
	gameMode := s.pickRandomMode()	
	language := mux.Vars(r)["lang"]

	alternatives := GenerateWordOptions(sentence, word)
	sentences.Sentences[i] = make(map[string]interface{})
	sentences.Sentences[i]["alternatives"] = alternatives
	sentences.Sentences[i]["question"] = word.English
	sentences.Sentences[i]["correct"] = word.Azerbaijani
	sentences.Sentences[i]["altCorrect"] = word.AltCorrect
	sentences.Sentences[i]["gameMode"] = gameMode
	sentences.Language = language

	return sentences
}

func getLevelNumber(level string) (int, error) {
	spliceLevel := strings.Split(level, "")
	var getLevel string

	for _, letter := range spliceLevel {
		getLevel = letter
	}

	levelNum, err := strconv.Atoi(getLevel)
	if err != nil {
		return 0, err
	}

	return levelNum, nil
}
