package ui

import (
	"fmt"
	"encoding/json"
	"net/http"
        "os"
)

var pixabayAPIKey = os.Getenv("PIXABAY_API")
const pixabayBaseURL = "https://pixabay.com/api/"

type PixabayResponse struct {
    Hits []struct {
        WebformatURL string `json:"webformatURL"`
    } `json:"hits"`
}

func SearchImages(query string) ([]string, error) {
    if pixabayAPIKey == "" {
        return nil, fmt.Errorf("access denied")
    }
    url := fmt.Sprintf("%s?key=%s&q=%s", pixabayBaseURL, pixabayAPIKey, query)
    resp, err := http.Get(url)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    var pixabayResp PixabayResponse
    if err := json.NewDecoder(resp.Body).Decode(&pixabayResp); err != nil {
        return nil, err
    }

    var imageURLs []string
    for _, hit := range pixabayResp.Hits {
        imageURLs = append(imageURLs, hit.WebformatURL)
		break
    }
    return imageURLs, nil
}
