package char

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

func VariationLoop(variations []string, specialCharacters map[string]string) []string {
	for _, variation := range variations {
		for _, r := range variation {
			char := string(r)
			if replacement, ok := specialCharacters[char]; ok {
				newWord := strings.Replace(variation, char, replacement, 1)

				variations = append(variations, newWord)

			}
		}

	}

	return variations
}

func GenerateVariations(word string) []string {
	var specialCharacters = map[string]string{}
	specialCharacters = map[string]string{
		"ə": "e",
		"ü": "u",
		"ö": "o",
		"ı": "i",
		"ğ": "g",
		"ş": "s",
		"ç": "c",
		"i": "i",
	}

	var variations = []string{}
	variations = append(variations, word)

	variations = VariationLoop(variations, specialCharacters)
	variations = VariationLoop(variations, specialCharacters)
	variations = VariationLoop(variations, specialCharacters)

	return variations
}

func AppendStringAtIndex(slice []string, value string, index int) []string {
	// Ensure the index is within the bounds of the slice
	if index < 0 || index > len(slice) {
		return slice // Return the original slice if the index is out of bounds
	}

	// Create a new slice with enough capacity to hold the additional element
	result := make([]string, len(slice)+1)

	// Copy the elements from the original slice up to the specified index
	copy(result, slice[:index])

	// Append the new value
	result[index] = value

	// Copy the remaining elements from the original slice
	copy(result[index+1:], slice[index:])

	return result
}

func ConvertToNum(word string) int {
	// Convert selected word count to int
	convertToNum, Err := strconv.Atoi(word)

	if Err != nil {
		fmt.Println("Error:", Err)
	} else {
		return convertToNum
	}

	return 0
}


func ConvertToString(word int) string {
	// Convert selected word count to int
	convertToNum := strconv.Itoa(word)

	return convertToNum
}

func ConvertToBool(word string) bool {
	// Convert string to boolean
	b, err := strconv.ParseBool(word)
	if err != nil {
		// Handle error if string cannot be converted to boolean
		fmt.Println("Error:", err)
		return true
	}

	return b

}

func GenerateSessionID() string {
	// Generate a random session ID (dummy example)
	const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	b := make([]byte, 32)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func FirstLetterToUpper(word string) string{
	leftOver := word[1:]
	word = strings.ToUpper(word[:1])
	word = word + leftOver

	return word
}

