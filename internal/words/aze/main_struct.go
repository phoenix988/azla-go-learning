package aze

import (
	"fmt"
	"strings"
)

var AllSentences = map[string][]Sentence{
	"stage1Level1": unit1SentencesLevel1,
	"stage1Level2": unit1SentencesLevel2,
	"stage1Level3": unit1SentencesLevel3,
	"stage1Level4": unit1SentencesLevel4,
	"stage1Level5": unit1SentencesLevel5,
	"stage2Level1": unit2SentencesLevel1,
	"stage2Level2": unit2SentencesLevel2,
	"stage2Level3": unit2SentencesLevel3,
	"stage2Level4": unit2SentencesLevel4,
	"stage2Level5": unit2SentencesLevel5,
	"stage3Level1": unit3SentencesLevel1,
	"stage3Level2": unit3SentencesLevel2,
	"stage3Level3": unit3SentencesLevel3,
	"stage3Level4": unit3SentencesLevel4,
	"stage3Level5": unit3SentencesLevel5,
}

var AllSentencesForStage = map[string]map[string][]Sentence{
	"stage1": {
		"Level1": unit1SentencesLevel1,
		"Level2": unit1SentencesLevel2,
		"Level3": unit1SentencesLevel3,
		"Level4": unit1SentencesLevel4,
		"Level5": unit1SentencesLevel5,
	},

	"stage2": {
		"Level1": unit2SentencesLevel1,
		"Level2": unit2SentencesLevel2,
		"Level3": unit2SentencesLevel3,
		"Level4": unit2SentencesLevel4,
		"Level5": unit2SentencesLevel5,
	},


	"stage3": {
		"Level1": unit3SentencesLevel1,
		"Level2": unit3SentencesLevel2,
		"Level3": unit3SentencesLevel3,
		"Level4": unit3SentencesLevel4,
		"Level5": unit3SentencesLevel5,
	},
}


// Define a function type
type Generate func() Sentence
type GenerateAll func() []Sentence
type GenerateCat func() string

type Sentence struct {
	Azerbaijani string `json:"aze"`
	English     string `json:"eng"`
	AltCorrect  string `json:"altCorrect"`
}

// Define a funcMap type
type LevelMap map[string]map[string]Generate
type ListAllMap map[string]map[string]GenerateAll
type StageMap map[string]GenerateCat

type All struct {
	Levels     map[string][]string
	Categories []string
}

func GenerateMain(cat, level string) (Sentence, error) {
	// Check if the category exists in the catMap
	if categoryGenerator, ok := catMap[cat]; ok {
		selectedCategory := categoryGenerator()
		if generator, ok := levelMap[selectedCategory][level]; ok {
			return generator(), nil
		}
	}

	return Sentence{}, fmt.Errorf("Category or level does not exist, %s, %s", cat, level) // Return error if category or level doesn't exist
}

func GenerateAllWords(cat, level string) ([]Sentence, error) {
	// Check if the category exists in the catMap
	if categoryGenerator, ok := catMap[cat]; ok {
		selectedCategory := categoryGenerator()
		if generator, ok := allMap[selectedCategory][level]; ok {
			return generator(), nil
		}
	}

	return []Sentence{}, fmt.Errorf("Category or level does not exist, %s, %s", cat, level) // Return error if category or level doesn't exist
}

// Initialize the funcMap
var levelMap = LevelMap{
	"Unit1": {
		"Level1": Unit1Level1,
		"Level2": Unit1Level2,
		"Level3": Unit1Level3,
		"Level4": Unit1Level4,
		"Level5": Unit1Level5,
	},

	"Unit2": {
		"Level1": Unit2Level1,
		"Level2": Unit2Level2,
		"Level3": Unit2Level3,
		"Level4": Unit2Level4,
		"Level5": Unit2Level5,
	},


	"Unit3": {
		"Level1": Unit3Level1,
		"Level2": Unit3Level2,
		"Level3": Unit3Level3,
		"Level4": Unit3Level4,
		"Level5": Unit3Level5,

	},
}

var allMap = ListAllMap{
	"Unit1": {
		"Level1": Unit1Level1All,
		"Level2": Unit1Level2All,
		"Level3": Unit1Level3All,
		"Level4": Unit1Level4All,
		"Level5": Unit1Level5All,
	},

	"Unit2": {
		"Level1": Unit2Level1All,
		"Level2": Unit2Level2All,
		"Level3": Unit2Level3All,
		"Level4": Unit2Level4All,
		"Level5": Unit2Level5All,
	},

	"Unit3": {
		"Level1": Unit3Level1All,
		"Level2": Unit3Level2All,
		"Level3": Unit3Level3All,
		"Level4": Unit3Level4All,
		"Level5": Unit3Level5All,
	},
}

var catMap = StageMap{
	"Unit1": func() string {
		return "Unit1"
	},

	"Unit2": func() string {
		return "Unit2"
	},

	"Unit3": func() string {
		return "Unit3"
	},
}

func GenerateAllLevels() *All {
	allLevels := &All{
		Levels:     make(map[string][]string),
		Categories: make([]string, 0, len(levelMap)),
	}

	// Populate Levels
	for cat, levels := range levelMap {
		allLevels.Categories = append(allLevels.Categories, cat)
		allLevels.Levels[cat] = make([]string, 0, len(levels))
		for level := range levels {
			allLevels.Levels[cat] = append(allLevels.Levels[cat], level)
		}
	}

	return allLevels

}


func removeSpecialChar(word string) string {
	word = strings.ReplaceAll(word, "?", "")
	word = strings.ReplaceAll(word, ",", "")

	return word
}


func GetWordBasedOnString(word string) Sentence{
  allSentences := returnAllSentences()
    for _, sentences := range allSentences {
        for _, sentence := range sentences {
			newWord := removeSpecialChar(strings.ToLower(word))

            // Check if the word exists in the Azerbaijani or English fields of the Sentence
            if removeSpecialChar(strings.ToLower(sentence.Azerbaijani)) == newWord || removeSpecialChar(strings.ToLower(sentence.English)) == strings.ToLower(newWord) {
                return sentence// Stop searching after finding the word in one of the sentences
            }
        }
    }

    return Sentence{}

}


func GetAllWords() []string{
  allSentences := returnAllSentences()
  uniqueWords := make(map[string]bool)

    for _, sentences := range allSentences {
        for _, sentence := range sentences {
	    // Split each sentence into words
            words := strings.Fields(sentence.Azerbaijani)
            words = append(words, strings.Fields(sentence.English)...)
            words = append(words, strings.Fields(sentence.AltCorrect)...)
            
            // Add each word to the uniqueWords map
            for _, word := range words {
                uniqueWords[word] = true
            }
        }
    }

    // Convert uniqueWords map keys to a slice
    allWords := make([]string, 0, len(uniqueWords))
    for word := range uniqueWords {
	    allWords = append(allWords, word)
    }

    return allWords

}

func GetAllSentences() []Sentence {
    allSentences := returnAllSentences()
    var allSentencesSlice []Sentence

    // Iterate through all sentences and collect them into a slice
    for _, sentences := range allSentences {
        allSentencesSlice = append(allSentencesSlice, sentences...)
    }

    return allSentencesSlice
}

func GetAllSentencesForStage(stage string, maxAmount int) []Sentence {
	allSentences := returnAllSentencesForStage(stage)
	sentencesList := []Sentence{}
	count := 0
	for _, sentences := range allSentences {
		for _, sentence := range sentences {
			sentencesList = append(sentencesList, sentence)
			
			if count == maxAmount {
				return sentencesList
			}

			count += 1
		}
	}

	return []Sentence{}
}


func returnAllSentences() map[string][]Sentence{
	return AllSentences
}

func returnAllSentencesForStage(stage string) map[string][]Sentence{
	return AllSentencesForStage[stage]
}

