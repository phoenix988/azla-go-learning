package aze

import (
	"math/rand"
	//"fmt"
	//"strings"
)

// unit == stage
// gonna change the naming convention to stage instead of unit

// stage 1 level 1
var unit1SentencesLevel1 = []Sentence{
		{Azerbaijani: "Salam, necəsiz?", English: "Hello, how are you?", AltCorrect: "Salamlar, necəsiz?"},
		{Azerbaijani: "Necə yasın var", English: "How old are you?"},
		{Azerbaijani: "Azərbaycan dil", English: "Azerbaijani language"},
		{Azerbaijani: "Pisyəm", English: "I am bad"},
		{Azerbaijani: "Görüşənə qədər", English: "See you later"},
		{Azerbaijani: "Çox sağ ol", English: "Thank you very much"},
		{Azerbaijani: "Nə vaxt?", English: "When?"},
		{Azerbaijani: "Hardansız?", English: "Where are you from?", AltCorrect: "Hardansən"},
		{Azerbaijani: "Mənim adım Konul", English: "My name is Konul"},
		{Azerbaijani: "Necəsən?", English: "How are you?", AltCorrect: "Necəsiz?"},
		{Azerbaijani: "Sən nə edirsən?", English: "What are you doing?", AltCorrect: "nə edirsən?"},
		{Azerbaijani: "Bu nədir?", English: "What is this?"},
		{Azerbaijani: "Mən yaxşıyam", English: "I am fine", AltCorrect: "yaxşıyam"},
		{Azerbaijani: "Sən hardansan?", English: "Where are you?", AltCorrect: "hardansan"},
		{Azerbaijani: "Onun adı Karl", English: "His name is Karl"},
		{Azerbaijani: "Hardan gəlirsin?", English: "Where are you coming from?", AltCorrect: "Hardansan?"},
  		{Azerbaijani: "Hardan bilirsən?", English: "How do you know?"},
		{Azerbaijani: "Niyə?", English: "Why?"},
		{Azerbaijani: "Nədir?", English: "What?"},
		{Azerbaijani: "Kimdir?", English: "Who?"},
  		
  		// Responses
  		{Azerbaijani: "Mən yaxşıyam", English: "I am fine", AltCorrect: "Yaxşıyam"},
  		{Azerbaijani: "Yaxşı görürəm", English: "I see"},
  		{Azerbaijani: "Çox xoşbəxtəm", English: "I am very happy"},
  		{Azerbaijani: "Tezim", English: "I am coming soon"},
	}

// First stage level 2
var unit1SentencesLevel2 = []Sentence{
		// Giving Directions
		{Azerbaijani: "Sola dön", English: "Turn left"},
		{Azerbaijani: "Sağa dön", English: "Turn right"},
		{Azerbaijani: "Duz gedin", English: "Go straight"},
		{Azerbaijani: "Geri qayıt", English: "Go back"},
		{Azerbaijani: "Burada durun", English: "Stop here"},
		{Azerbaijani: "İlk kənarda sola dön", English: "Turn left at the first corner"},
		{Azerbaijani: "İlk kənarda sağa dön", English: "Turn right at the first corner"},
		{Azerbaijani: "İndi sol tərəfə dön", English: "Now turn left"},
		{Azerbaijani: "Yola düz davam edin", English: "Continue straight on the road"},
		{Azerbaijani: "Bu yoldan gedərək getmək olmaz", English: "You can't go through this road"},
		{Azerbaijani: "Qarşıdan sağa dön", English: "Turn right from across"},
		{Azerbaijani: "Sağ tərəfə dön", English: "Turn right"},
		{Azerbaijani: "Sol tərəfə dön", English: "Turn left"},
		{Azerbaijani: "Yan yola dönün", English: "Turn onto the side road"},
		{Azerbaijani: "İndi döngələrə girin", English: "Now enter the roundabouts"},
		{Azerbaijani: "İndi yola girin", English: "Now enter the road"},
		{Azerbaijani: "Bu istiqamətdə yola davam edin", English: "Continue on this direction"},
		{Azerbaijani: "Üçüncü çevrilişdə sağa dön", English: "Turn right at the third roundabout"},
		{Azerbaijani: "Yolun sonuna qədər davam edin", English: "Continue to the end of the road"},
	}


// First stage level 3
var unit1SentencesLevel3 = []Sentence{
		// Expressing Emotions
		{Azerbaijani: "Mən qəzəbləndim", English: "I am angry"},
    	{Azerbaijani: "Mən təəssüf edirəm", English: "I am sorry"},
    	{Azerbaijani: "Mənə də pis gəldi", English: "I am upset too"},
    	{Azerbaijani: "Mən sevinmişəm", English: "I am glad"},
    	{Azerbaijani: "Mən xəyalım keçib", English: "I am daydreaming"},
    
		// Asking for Help
		{Azerbaijani: "Mənə kömək edə bilərsən?", English: "Can you help me?", AltCorrect: "Mənə kömək edə bilərsən?"},
		{Azerbaijani: "Mən nə edəcəyimi bilmirəm", English: "I don't know what to do", AltCorrect: "edəcəyimi bilmirəm"},
		{Azerbaijani: "Zəhmət olmasa mənə göstər", English: "Please show me", AltCorrect: "Zəhmət olmasa mənə göstər"},
		{Azerbaijani: "Mənə izah edin", English: "Please explain to me", AltCorrect: "izah edin"},
		{Azerbaijani: "Mən nə etməliyəm?", English: "What should I do?", AltCorrect: "nə etməliyəm?"},
	}

// First stage level 4
var unit1SentencesLevel4 = []Sentence{
		{Azerbaijani: "Salam, necəsiz?", English: "Hello, how are you?", AltCorrect: "Salamlar, necəsiz?"},
		{Azerbaijani: "Necə yasın var", English: "How old are you?", AltCorrect: "Necə yasın var"},
		{Azerbaijani: "Azərbaycan dil", English: "Azerbaijani language", AltCorrect: "Azərbaycan dil"},
		{Azerbaijani: "Pisyəm", English: "I am bad", AltCorrect: "Pisyəm"},
		{Azerbaijani: "Görüşənə qədər", English: "See you later", AltCorrect: "Görüşənə qədər"},
		{Azerbaijani: "Çox sağ ol", English: "Thank you very much", AltCorrect: "Çox sağ ol"},
		{Azerbaijani: "Nə vaxt?", English: "When?", AltCorrect: "Nə vaxt?"},
		{Azerbaijani: "Hardansız?", English: "Where are you from?", AltCorrect: "Hardansən"},
		{Azerbaijani: "Mənim adım Konul", English: "My name is Konul", AltCorrect: "Mənim adım Konul"},
		{Azerbaijani: "Necəsən?", English: "How are you?", AltCorrect: "Necəsiz?"},
		{Azerbaijani: "Sən nə edirsən?", English: "What are you doing?", AltCorrect: "nə edirsən?"},
		{Azerbaijani: "Bu nədir?", English: "What is this?"},
		{Azerbaijani: "Mən yaxşıyam", English: "I am fine", AltCorrect: "yaxşıyam"},
		{Azerbaijani: "Sən hardansan?", English: "Where are you?", AltCorrect: "hardansan"},
		{Azerbaijani: "Onun adı Karl", English: "His name is Karl"},
	}

// First stage level 5
var unit1SentencesLevel5 = []Sentence{
		{Azerbaijani: "Salam, necəsiz?", English: "Hello, how are you?", AltCorrect: "Salamlar, necəsiz?"},
		{Azerbaijani: "Necə yasın var", English: "How old are you?"},
		{Azerbaijani: "Azərbaycan dil", English: "Azerbaijani language"},
		{Azerbaijani: "Pisyəm", English: "I am bad"},
		{Azerbaijani: "Görüşənə qədər", English: "See you later"},
		{Azerbaijani: "Çox sağ ol", English: "Thank you very much"},
		{Azerbaijani: "Nə vaxt?", English: "When?"},
		{Azerbaijani: "Hardansız?", English: "Where are you from?", AltCorrect: "Hardansən"},
		{Azerbaijani: "Mənim adım Konul", English: "My name is Konul"},
		{Azerbaijani: "Necəsən?", English: "How are you?", AltCorrect: "Necəsiz?"},
		{Azerbaijani: "Sən nə edirsən?", English: "What are you doing?", AltCorrect: "nə edirsən?"},
		{Azerbaijani: "Bu nədir?", English: "What is this?"},
		{Azerbaijani: "Mən yaxşıyam", English: "I am fine", AltCorrect: "yaxşıyam"},
		{Azerbaijani: "Sən hardansan?", English: "Where are you?", AltCorrect: "hardansan"},
		{Azerbaijani: "Onun adı Karl", English: "His name is Karl"},
	}

func Unit1Level1() Sentence {
	sentences := unit1SentencesLevel1
	lenghtOfSentence := len(sentences)
	randomNum := rand.Intn(lenghtOfSentence)

	// Return a random sentence from the list
	return sentences[randomNum]
}


func Unit1Level1All() []Sentence {
	sentences := unit1SentencesLevel1

	return sentences
}

func Unit1Level2() Sentence{
	sentences := unit1SentencesLevel2
	lenghtOfSentence := len(sentences)
	randomNum := rand.Intn(lenghtOfSentence)

	// Return a random sentence from the list
	return sentences[randomNum]
}

func Unit1Level2All() []Sentence {
	sentences := unit1SentencesLevel2

	return sentences
}

func Unit1Level3() Sentence{
	sentences := unit1SentencesLevel3
	lenghtOfSentence := len(sentences)
	randomNum := rand.Intn(lenghtOfSentence)

	// Return a random sentence from the list
	return sentences[randomNum]
}

func Unit1Level3All() []Sentence {
	sentences := unit1SentencesLevel3

	return sentences
}

func Unit1Level4() Sentence{
	sentences := unit1SentencesLevel4
	lenghtOfSentence := len(sentences)
	randomNum := rand.Intn(lenghtOfSentence)

	// Return a random sentence from the list
	return sentences[randomNum]
}

func Unit1Level4All() []Sentence {
	sentences := unit1SentencesLevel4

	return sentences
}

func Unit1Level5() Sentence{
	sentences := unit1SentencesLevel5
	lenghtOfSentence := len(sentences)
	randomNum := rand.Intn(lenghtOfSentence)
	
	// Return a random sentence from the list
	return sentences[randomNum]
}

func Unit1Level5All() []Sentence {
	sentences := unit1SentencesLevel5

	return sentences
}
