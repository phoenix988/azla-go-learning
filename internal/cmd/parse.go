package cmd

import (
	"flag"
	"fmt"
	"os"
)

// Dynamic config that you can change
type AzlaConfig struct {
	Port       string // Port number of server
	DataPath   string
	Help       bool
	DB         string
	DBUser     string
	DBPassword string
	MaxAmount  string
}

var azlaConfig AzlaConfig

// Parse command line arguments
func ParseFlag() AzlaConfig {
	flag.StringVar(&azlaConfig.Port, "p", "3000", "Port Nummer to listen on")
	flag.StringVar(&azlaConfig.Port, "port", "3000", "Port Nummer to listen on")
	flag.StringVar(&azlaConfig.DataPath, "data", "data/", "Data Path")
	flag.StringVar(&azlaConfig.MaxAmount, "max", "12", "Max amount of words")
	flag.StringVar(&azlaConfig.DB, "db", "localhost:5432", "db connection")
	flag.StringVar(&azlaConfig.DBUser, "dbuser", "postgres", "db connection")
	flag.StringVar(&azlaConfig.DBPassword, "dbpass", "password", "db connection")
	flag.BoolVar(&azlaConfig.Help, "help", false, "Print Help Information")

	flag.Parse()

	if azlaConfig.Help {
		fmt.Println("Usage:")
		flag.PrintDefaults()
		os.Exit(0)
	}

	return azlaConfig
}

// Retrieve the data
func GetFlag() AzlaConfig {
	return azlaConfig
}
