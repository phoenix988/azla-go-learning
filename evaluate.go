package main

import (
	"net/http"
	"regexp"
	"strings"
)

func (s *APIServer) evaluateEmail(email string) (bool, error) {
	accounts, err := s.store.GetAccounts()
	if err != nil {
		return false, err
	}

	var emails []string
	for _, acc := range accounts {
		emails = append(emails, acc.Email)
		if email == acc.Email {
			return false, nil // Return false if email exist
		}
	}

	if isEmail(email) == false { // check if you enter an email
		return false, nil
	}

	return true, nil
}

func (s *APIServer) evaluatePassword(password, confirm string) bool {
	if password == confirm {
		return true
	}

	return false
}

func (s *APIServer) evaluateName(firstName, lastName string) bool {
	if firstName == "" {
		return false
	} else if lastName == "" {
		return false
	}

	return true
}

func (s *APIServer) evaluateUserName(username string) bool {
	if username == "" {
		return false
	}

	accounts, err := s.store.GetAccounts()
	if err != nil {
		return false
	}

	var usernames []string
	for _, user := range accounts {
		usernames = append(usernames, user.UserName)
		if username == user.UserName {
			return false // Return false if email exist
		}
	}

	return true
}

func (s *APIServer) evaluateCreateAccount(createAccountReq *CreateAccountRequest, w http.ResponseWriter) (bool, error) {
	checkEmail, _ := s.evaluateEmail(createAccountReq.Email)
	checkPassword := s.evaluatePassword(createAccountReq.Password, createAccountReq.ConfirmPassword)
	checkName := s.evaluateName(createAccountReq.FirstName, createAccountReq.LastName)
	checkUserName := s.evaluateUserName(createAccountReq.UserName)
	evaluateOk := map[string]bool{}

	if checkEmail == false { // Email Already exist
		evaluateOk["emailFail"] = true
	} 
	if checkPassword == false {
		evaluateOk["passwordFail"] = true
	} 
	if checkName == false {
		evaluateOk["nameFail"] = true
	} 
	if checkUserName == false {
		evaluateOk["usernameFail"] = true
	}

	if len(evaluateOk) != 0 {
		return false, WriteJSON(w, http.StatusOK, evaluateOk)
	}

	return true, nil
}

//func (s *APIServer) evaluate(answer, correct string, r *http.Request) bool {
//	variations := char.GenerateVariations(correct)
//	for _, alternateCorrect := range variations {
//		if answer == strings.ToLower(alternateCorrect) {
//			s.UpdateResults(r, "correct")
//			return true
//		}
//	}
//
//	if strings.ToLower(answer) == strings.ToLower(correct) {
//		s.UpdateResults(r, "correct")
//		return true
//	}
//
//	s.UpdateResults(r, "incorrect")
//	return false
//}

func (s *APIServer) evaluateWordList(name string, r *http.Request) (bool, error) {
	id, err := getID(r)
	if err != nil {
		return false, err
	}

	account, err := s.store.GetAccountByID(id)
	if err != nil {
		return false, err
	}

	words, _ := s.store.GetWordList(account.Email)
	for _, wordList := range words {
		if wordList.WordListName == name {
			return true, nil
		}
	}

	return false, nil

}

func (s *APIServer) evaluateSession(id int, allSessions []*SaveSession, updateSessionIDReq int) (*Account, error) {
	sessionID := UpdateSessionID(updateSessionIDReq)

	var sessions []int
	for _, ses := range allSessions {
		sessions = append(sessions, ses.UniqueID)
		if updateSessionIDReq == ses.UniqueID {
			if err := s.store.UpdateSessionID(id, sessionID); err != nil {
				return nil, err
			}

			err := s.store.UpdateSessionID(id, sessionID)

			if err != nil {
				return nil, err
			}

			return sessionID, err

		}
	}

	return nil, nil

}

func (s *APIServer) evaluateSessionWordList(list string, r *http.Request) (bool, error) {
	account, err := s.getAccountByID(r)
	if err != nil {
		return false, err
	}

	activeSession, err := s.store.GetSessionByID(account.SessionID)
	if err != nil {
		return false, err
	}

	if list == activeSession.WordList {
		return true, nil
	}

	return false, nil
}

func containsMeaningfulContent(value string) bool {
	// Check if the value contains any non-escape characters
	return strings.ContainsAny(value, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
}

func isEmail(email string) bool {
	regex := `^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`
	match, err := regexp.MatchString(regex, email)
	if err != nil {
		return false
	}
	return match
}
