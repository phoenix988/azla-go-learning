package main

import (
	"fmt"
	"azla_go_learning/internal/ui"
)

func loadImage(searchTerm string) (string, error){
	imageURLs, err := ui.SearchImages(searchTerm)
	if err != nil {
		fmt.Println("Error:", err)
		return "", err
	}

	var selectedUrl string
	for _, url := range imageURLs {
		selectedUrl = url
		break
	}

	return selectedUrl, err

}
