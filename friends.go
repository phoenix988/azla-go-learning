package main


import (
	"net/http"
	"encoding/json"
)


func(s *APIServer) handleFriends(w http.ResponseWriter, r *http.Request, ) error {
	switch r.Method {
		case "GET":
			return s.handleGetFriends(w, r)
		case "POST":
			return s.handleCreateFriends(w, r)
		case "PUT":
			return s.handleUpdateFriendList(w, r)
	}

	return nil
}


func(s *APIServer) handleUpdateFriendList(w http.ResponseWriter, r *http.Request) error {
	//account, err := s.getAccountByID(r)
	//if err != nil {
	//	return err
	//}

	//friends, err := s.store.GetFriends(account.ID)
	//if err != nil {
	//	return err
	//}


	friendListReq := new(UpdateFriendListRequest)
	if err := json.NewDecoder(r.Body).Decode(&friendListReq); err != nil {
		return err
	}
	
	return WriteJSON(w, http.StatusOK, friendListReq.Friend)
}

func(s *APIServer) handleGetFriends(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	friends, err := s.store.GetFriends(account.ID)
	if err != nil {
		return err
	}


	return WriteJSON(w, http.StatusOK, friends)
}


func(s *APIServer) handleCreateFriends(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	friends := NewFriendList(account.ID)

	err = s.store.CreateFriendList(friends)
	if err != nil {
		return WriteJSON(w, http.StatusNotAcceptable, "list for user exist")
	}
	
	return WriteJSON(w, http.StatusOK, friends)
}
