package main

import (
	//"math/rand"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type CreateAccountRequest struct {
	FirstName       string `json:"firstName"`
	LastName        string `json:"lastName"`
	Email           string `json:"email"`
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirmPassword"`
	UserName        string `json:"username"`
}



type CreateWordListRequest struct {
	WordListName string            `json:"wordListName"`
	Words        map[string]string `json:"words"`
}

type UpdatePRPRequest struct {
	ProfilePicture string `json:"profilePicture"`
}

type UpdateFriendListRequest struct {
	UserId int `json:"userId"`
	Email string `json:"email"`
	Friend string `json:"friend"`
}

type UpdatePasswordRequest struct {
	CurrentPassword string `json:"currentPassword"`
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirmPassword"`
}

type UpdateUsernameRequest struct {
	UserName string `json:"username"`
}

type UpdateEmailRequest struct {
	Email string `json:"email"`
}

type UpdateNameRequest struct {
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
}

type AccountSettingRequest struct {
	Language string `json:"language"`
}

type UpdateWordListRequest struct {
	Words        map[string]string `json:"words"`
	WordListName string            `json:"wordListName"`
}

type MaxAmountWordRequest struct {
	MaxAmount string `json:"maxAmountWords"`
}

type EvaluateAnswerRequest struct {
	Answer string `json:"answer"`
}

type EvaluateCreateAccount struct {
	Email    bool `json:"email"`
	Password bool `json:"-"`
	Name     bool `json:"name"`
	UserName bool `json:"username"`
}

type EvaluateAnswer struct {
	Answer       string
	Correct      string
	LastQuestion bool
	Session      *SaveSession
	SessionId    int
	Account      *Account
}

type Friends struct {
	ID int `json:"id"`
	UserId int `json:"userId"`
	FriendRequests []string `json:"friendRequests"`
	FriendList []string `json:"friendList"`
	BlockedUsers []string `json:"blockedUsers"`
}

type JumpToQuestionRequest struct {
	ToQuestion int `json:"toQuestion"`
}

type LoginResponse struct {
	Email string `json:"email"`
	Token string `json:"token"`
}

type LoginRequest struct {
	UserName string `json:"username"`
	Password string `json:"password"`
}

type UpdateSessionIDRequest struct {
	SessionID int `json:"sessionID"`
}

type UpdateProgressRequest struct {
	Lesson int `json:"lesson"`
}

type NewSessionRequest struct {
	Lesson            string      `json:"lesson"`
	NumberOfQuestions int         `json:"numberOfQuestions"`
	Words             interface{} `json:"words"`
}

type ReviewMistakeRequest struct {
	NumberOfQuestions int      `json:"numberOfQuestions"`
	Words             []string `json:"words"`
}

type UserProgress struct {
	Progress map[string]map[int]map[int]bool // stage -> level -> lesson -> completed
}

// data stored in the accounts table in the database
type Account struct {
	ID        int       `json:"id"`
	FirstName string    `json:"firstname"`
	LastName  string    `json:"lastname"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"createdAt"`
	Password  string    `json:"-"`
	SessionID int       `json:"sessionID"`
	UserName  string    `json:"username"`
	PhoneNumber  int    `json:"phonenumber"`
}

func (a *Account) ValidPassword(pw string) bool {
	return bcrypt.CompareHashAndPassword([]byte(a.Password), []byte(pw)) == nil
}

// data stored in the account settings table
type AccountSettings struct {
	ID              int    `json:"id"`
	UserID          int    `json:"userID"`
	DarkMode        bool   `json:"darkmode"`
	ProfilePicture  string `json:"profilePicture"`
	DefaultLanguage string `json:"defaultLanguage"`
	IsNewAccount    bool   `json:"isNewAccount"`
}

// data for new session when the user starts a new round
// some data will later be exported so you can see history,
// progression etc and move to the next level
type SaveSession struct {
	ID                   int                            `json:"id"`
	Email                string                         `json:"email"`
	Words                []string                       `json:"words"`
	Correct              []string                       `json:"correct"`
	UniqueID             int                            `json:"uniqueID"`
	CorrectAnswers       int                            `json:"correctAnswers"`
	IncorrectAnswers     int                            `json:"incorrectAnswers"`
	MaxAmountWords       string                         `json:"maxAmountWords"`
	CurrentQuestionImage string                         `json:"currentQuestionImage"`
	Answers              []string                       `json:"answers"`
	CurrentQuestion      int                            `json:"currentQuestion"`
	WordList             string                         `json:"wordList"`
	Sentences            map[int]map[string]interface{} `json:"sentences"`
	Health               int                            `json:"health"`
	Mode                 string                         `json:"mode"`
	CreatedAt            time.Time                      `json:"createdAt"`
	IsCompleted          bool                           `json:"iscompleted"`
	SelectedLevel        map[string]map[string]string   `json:"selectedLevel"`
	Language             string                         `json:"language"`
}

type Sentences struct {
	Sentences  map[int]map[string]interface{} `json:"sentences"`
	Level      string                         `json:"level"`
	Section    string                         `json:"section"`
	LanguageID int                            `json:"languageId"`
	Unit       string                         `json:"unit"`
}

type Progress struct {
	ID       int                    `json:"id"`
	UserName string                 `json:"username"`
	UserID   int                    `json:"userID"`
	Level    map[string]interface{} `json:"level"`
	Language string                 `json:"language"`
}

type CustomWordList struct {
	ID           int               `json:"id"`
	UserName     string            `json:"userName"`
	WordListName string            `json:"wordListName"`
	Words        map[string]string `json:"words"`
}

type QuizSession struct {
	CurrentWord          string
	CurrentCorrect       string
	CurrentQuestionImage string
	CurrentQuestion      int
	MaxAmountWords       int
	Sentences            map[string]interface{}
}

type Result struct {
	CorrectAnswers   int `json:"correctAnswers"`
	IncorrectAnswers int `json:"incorrectAnswers"`
	MaxAmountWords   int `json:"maxAmountWords"`
}

type GenerateSentence struct {
	Sentences map[int]map[string]interface{} `json:"sentences"`
	Language  string                         `json:"language"`
}

type Languages struct{}

func UpdateAccountName(firstName, lastName string) *Account {
	return &Account{
		FirstName: firstName,
		LastName:  lastName,
	}
}

func UpdatePassword(password string) *Account {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return &Account{
		Password: string(hashedPassword),
	}
}

func UpdateEmail(email string) *Account {
	return &Account{
		Email: email,
	}
}

func UpdateUserName(username string) *Account {
	return &Account{
		UserName: username,
	}
}

func UpdateSessionID(sessionID int) *Account {
	return &Account{
		SessionID: sessionID,
	}

}

func NewSessionOld(email, wordList,
	maxAmountWords string,
	words, correct []string,
	uniqueID int) *SaveSession {
	return &SaveSession{
		Email:            email,
		Words:            words,
		Correct:          correct,
		UniqueID:         uniqueID,
		CorrectAnswers:   0,
		IncorrectAnswers: 0,
		MaxAmountWords:   maxAmountWords,
		Answers:          []string{},
		CurrentQuestion:  0,
		WordList:         wordList,
		Sentences:        map[int]map[string]interface{}{},
	}

}

func NewSession(email, maxAmount, mode, stage, level, lesson string,
	uniqueID int, sent *GenerateSentence) *SaveSession {
	return &SaveSession{
		Email:            email,
		Words:            []string{},
		Correct:          []string{},
		UniqueID:         uniqueID,
		CorrectAnswers:   0,
		IncorrectAnswers: 0,
		MaxAmountWords:   maxAmount,
		Answers:          []string{},
		CurrentQuestion:  1,
		WordList:         "",
		Sentences:        sent.Sentences,
		Health:           5,
		Mode:             mode,
		CreatedAt:        time.Now().UTC(),
		IsCompleted:      false,
		SelectedLevel: map[string]map[string]string{
			stage: {
				level: lesson,
			},
		},
		Language: sent.Language,
	}
}

func NewWordList(username, name string, words map[string]string) *CustomWordList {
	return &CustomWordList{
		UserName:     username,
		WordListName: name,
		Words:        words,
	}
}

func NewAccount(firstName, lastName, email, password, username string) (*Account, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	return &Account{
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Password:  string(hashedPassword),
		CreatedAt: time.Now().UTC(),
		UserName:  username,
	}, nil

}

func NewAccountSettings(id int, language string) *AccountSettings {
	return &AccountSettings{
		UserID:          id,
		DarkMode:        false,
		ProfilePicture:  "",
		DefaultLanguage: language,
		IsNewAccount:    true,
	}
}


func NewFriendList(userId int) *Friends {
	return &Friends{
		UserId: userId,
		FriendList: []string{},
		FriendRequests: []string{},
		BlockedUsers: []string{},
	}
}

func NewProgress(username string, id int, level map[string]interface{}, lang string) *Progress {
	return &Progress{
		UserName: username,
		UserID:   id,
		Level:    level,
		Language: lang,
	}
}
