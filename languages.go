package main

import (
	"azla_go_learning/internal/words/aze"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func (l *Languages) AzeSessionNormal(r *http.Request, w http.ResponseWriter, s *APIServer) error {
	level := mux.Vars(r)["level"]
	cat := mux.Vars(r)["cat"]

	lessonReq := new(NewSessionRequest)
	if err := json.NewDecoder(r.Body).Decode(&lessonReq); err != nil {
		return err
	}

	sentences := &GenerateSentence{
		Sentences: make(map[int]map[string]interface{}),
	}

	if lessonReq.NumberOfQuestions == 0 {
		lessonReq.NumberOfQuestions = 15
	}

	for i := 1; i <= lessonReq.NumberOfQuestions; i++ {
		sentence, err := aze.GenerateAllWords(cat, level)
		if err != nil {
			return err
		}

		selected, err := aze.GenerateMain(cat, level)
		if err != nil {
			return err
		}

		err = checkForUnlocked(s, r, lessonReq, cat, level)
		if err != nil {
			return err
		}

		sentences = s.CreateSessionAlternatives(r, sentence, sentences, i, selected)

	}

	switch r.Method {
	case "POST":
		identifier, err := startQuiz(s, r, lessonReq, sentences, "drag_drop")
		if err != nil {
			return err
		}

		sentences.Sentences[0] = make(map[string]interface{})
		sentences.Sentences[0]["id"] = identifier

		return WriteJSON(w, http.StatusOK, sentences)
	default:
		return fmt.Errorf("method not supported %s", r.Method)
	}
}

func (l *Languages) AzeSessionReview(r *http.Request, w http.ResponseWriter, s *APIServer) error {
	reviewReq := new(NewSessionRequest)
	if err := json.NewDecoder(r.Body).Decode(&reviewReq); err != nil {
		return err
	}

	sentences := &GenerateSentence{
		Sentences: make(map[int]map[string]interface{}),
	}

	var wordsStrings []string
	switch words := reviewReq.Words.(type) {
	case []string:
	case []int:
	case []interface{}:
		for _, word := range words {
			// Attempt to assert each element to a string
			if str, ok := word.(string); ok {
				// If the assertion is successful, append the string to wordsStrings
				wordsStrings = append(wordsStrings, str)
			} else {
				// Handle the case where the assertion fails
				fmt.Printf("Unable to convert %v to string\n", word)
			}
		}
		// Now you can use words as a slice of interfaces
	default:
		// Handle other types or the case where reviewReq.Words is not a slice
		fmt.Println("Unknown type or not a slice")
	}

	for i, word := range wordsStrings {
		selected := aze.GetWordBasedOnString(word)

		sentence := aze.GetAllSentences()

		sentences = s.CreateSessionAlternatives(r, sentence, sentences, i, selected)

	}

	switch r.Method {
	case "POST":
		sentences.Sentences[0] = make(map[string]interface{})
		sentences.Sentences[0]["review"] = true
		identifier, err := startQuiz(s, r, reviewReq, sentences, "drag_drop")
		if err != nil {
			return err
		}

		sentences.Sentences[0]["id"] = identifier

		return WriteJSON(w, http.StatusOK, sentences)

	default:
		return fmt.Errorf("method not supported %s", r.Method)
	}
}

func (l *Languages) AzeSessionSkip(r *http.Request, w http.ResponseWriter, s *APIServer) error {
	reviewReq := new(NewSessionRequest)
	if err := json.NewDecoder(r.Body).Decode(&reviewReq); err != nil {
		return err
	}

	stage := mux.Vars(r)["cat"]

	sentences := &GenerateSentence{
		Sentences: make(map[int]map[string]interface{}),
	}

	words := aze.GetAllSentencesForStage(stage, reviewReq.NumberOfQuestions)

	for i, word := range words {
		sentence := aze.GetAllSentences()

		sentences = s.CreateSessionAlternatives(r, sentence, sentences, i, word)

	}

	switch r.Method {
	case "POST":
		sentences.Sentences[0] = make(map[string]interface{})
		sentences.Sentences[0]["skipsession"] = true
		identifier, err := startQuiz(s, r, reviewReq, sentences, "drag_drop")
		if err != nil {
			return err
		}

		sentences.Sentences[0]["id"] = identifier

		return WriteJSON(w, http.StatusOK, sentences)

	default:
		return fmt.Errorf("method not supported %s", r.Method)
	}
}
