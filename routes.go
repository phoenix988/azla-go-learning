package main

import (
	"github.com/gorilla/mux"
)

func (s *APIServer) CreateMainRoutes(router *mux.Router) *mux.Router{
	// Account related handlers
	router.HandleFunc("/account/{id}", withJWTAuth(makeHTTPHandleFunc(s.handleGetAccountByID), s.store))
	router.HandleFunc("/account/{id}/update/{action}", withJWTAuth(makeHTTPHandleFunc(s.handleUpdateAccount), s.store))
	router.HandleFunc("/account/{id}/settings", withJWTAuth(makeHTTPHandleFunc(s.handleAccountSettings), s.store))
	router.HandleFunc("/account/{id}/settings/{attribute}/{update}", withJWTAuth(makeHTTPHandleFunc(s.handleUpdateAccountSettings), s.store))
	router.HandleFunc("/email/{string}", withJWTAuthEmail(makeHTTPHandleFunc(s.handleGetAccountByEmail), s.store))
	router.HandleFunc("/username/{string}", withJWTAuthEmail(makeHTTPHandleFunc(s.handleGetAccountByUserName), s.store))
	
	// requires no authentication
	router.HandleFunc("/login", makeHTTPHandleFunc(s.handleLogin)) // login handler
	router.HandleFunc("/logout", makeHTTPHandleFunc(s.handleAccountLogout)) // logout handler
	router.HandleFunc("/account", makeHTTPHandleFunc(s.handleAccount))
	router.HandleFunc("/user/{username}", makeHTTPHandleFunc(s.handleIfUserExist))
	router.HandleFunc("/email/check/{email}", makeHTTPHandleFunc(s.handleIfEmailExist))

	// Session management
	router.HandleFunc("/account/{id}/sessions", withJWTAuth(makeHTTPHandleFunc(s.handleGetAllSessions),s.store))
	router.HandleFunc("/account/{id}/sessions/get/{string}", withJWTAuth(makeHTTPHandleFunc(s.handleGetRecentMistakesFromSession),s.store))
	router.HandleFunc("/account/{id}/sessions/update/{session}", withJWTAuth(makeHTTPHandleFunc(s.handleUpdateSessionIsComplete), s.store))
	router.HandleFunc("/account/{id}/sessions/{session}", withJWTAuth(makeHTTPHandleFunc(s.handleGetSessionByID), s.store))
	router.HandleFunc("/account/{id}/sessions/{session}/answers", withJWTAuth(makeHTTPHandleFunc(s.handleGetSessionAnswers), s.store))

	// Friends / social management
	router.HandleFunc("/account/{id}/friends", withJWTAuth(makeHTTPHandleFunc(s.handleFriends), s.store))


	// Quiz Generation handlers
	router.HandleFunc("/words/api/{id}/{lang}/{cat}/{level}", withJWTAuth(makeHTTPHandleFunc(s.handleGenerateSession), s.store))
	router.HandleFunc("/words/api/{id}/{lang}/custom/review/mistake", withJWTAuth(makeHTTPHandleFunc(s.handleGenerateReviewSession), s.store))
	router.HandleFunc("/words/api/{id}/{lang}/skip/stages/{cat}", withJWTAuth(makeHTTPHandleFunc(s.handleGenerateSkipSession), s.store))
	router.HandleFunc("/words/api/{id}/{lang}/{question}/ses/{session}", withJWTAuth(makeHTTPHandleFunc(s.handleGetQuestionBySessionID), s.store))
	router.HandleFunc("/words/api/{id}/{lang}/{question}", withJWTAuth(makeHTTPHandleFunc(s.handleGetQuestionByCurrent), s.store))
	router.HandleFunc("/words/api/search/{word}", makeHTTPHandleFunc(s.handleGetImage))
	
	// Validate the current question
	router.HandleFunc("/words/api/{id}/{lang}/{question}/val/run", withJWTAuth(makeHTTPHandleFunc(s.handleEvaluateQuestion), s.store))
	router.HandleFunc("/words/api/{id}/{lang}/{question}/{session}/val/run", withJWTAuth(makeHTTPHandleFunc(s.handleEvaluateQuestionBySessionID), s.store))
	// Update session result
	router.HandleFunc("/words/api/{id}/{lang}/{question}/update_result/{string}", withJWTAuth(makeHTTPHandleFunc(s.handleUpdateResult), s.store))
	router.HandleFunc("/words/api/{id}/{lang}/{question}/update_result/{string}/{session}", withJWTAuth(makeHTTPHandleFunc(s.handleUpdateResultBySessionID), s.store))

	// Handlers Related to progress
	router.HandleFunc("/progress/api/{id}", withJWTAuth(makeHTTPHandleFunc(s.handleProgress), s.store))
	router.HandleFunc("/progress/api/{id}/get/all", withJWTAuth(makeHTTPHandleFunc(s.handleGetAllProgress), s.store))
	router.HandleFunc("/progress/api/{id}/delete/all", withJWTAuth(makeHTTPHandleFunc(s.handleDeleteAllProgress), s.store))
	router.HandleFunc("/progress/api/{id}/reset/all", withJWTAuth(makeHTTPHandleFunc(s.handleResetAllProgress), s.store))
	router.HandleFunc("/progress/api/{id}/get/confirm", withJWTAuth(makeHTTPHandleFunc(s.handleIfAccountHaveLanguages), s.store))
	router.HandleFunc("/progress/api/{id}/{lang}", withJWTAuth(makeHTTPHandleFunc(s.handleGetProgressByLanguage), s.store))

	router.HandleFunc("/words/api/aze/drag/{id}/{cat}/{level}", withJWTAuth(makeHTTPHandleFunc(s.handleGenerateSession), s.store))
	router.HandleFunc("/words/api/aze/all", makeHTTPHandleFunc(s.handleGetAllAzeLevels))
	router.HandleFunc("/words/api/update/result/test/{id}/{string}", withJWTAuth(makeHTTPHandleFunc(s.handleUpdateResult), s.store))
	router.HandleFunc("/words/api/update_result/session/{session}/{string}", withJWTAuthSession(makeHTTPHandleFunc(s.handleUpdateResultBySessionID), s.store))

	// Wordlist related handlers
	router.HandleFunc("/words/api", makeHTTPHandleFunc(s.handleGetWords))
	router.HandleFunc("/words/api/list", makeHTTPHandleFunc(s.handleGetWordList))
	router.HandleFunc("/words/api/{string}", makeHTTPHandleFunc(s.handleGetWordsByName))
	router.HandleFunc("/words/api/{string}/amount", makeHTTPHandleFunc(s.handleGetWordsAmount))
	router.HandleFunc("/words/api/{id}/add", makeHTTPHandleFunc(s.handleCreateWordList))
	router.HandleFunc("/words/api/{id}/get", makeHTTPHandleFunc(s.handleGetCustomWordList))
	router.HandleFunc("/words/api/{id}/update", makeHTTPHandleFunc(s.handleUpdateCustomWordList))

	// Quiz Related Handlers ("working on improving this, so will be removed soon")
	router.HandleFunc("/quiz/{id}/{string}/{question}", makeHTTPHandleFunc(s.handleStartQuiz))
	router.HandleFunc("/quiz/{id}/{string}/{question}/prev", makeHTTPHandleFunc(s.handlePrevQuiz))
	router.HandleFunc("/quiz/{id}/{string}/{question}/jump", makeHTTPHandleFunc(s.handleJumpQuiz))
	//router.HandleFunc("/quiz/{id}/{string}/{question}/submit", makeHTTPHandleFunc(s.handleSubmitQuiz))
	router.HandleFunc("/quiz/{id}/{string}/{question}/update_answer", makeHTTPHandleFunc(s.handleUpdateQuestion))
	router.HandleFunc("/quiz/{id}/{string}/{question}/update_question", makeHTTPHandleFunc(s.handleUpdateQuestion))
	router.HandleFunc("/quiz/{id}/{string}/{question}/answer", makeHTTPHandleFunc(s.handleUpdateAnswer))
	router.HandleFunc("/quiz/{id}/{string}/end", makeHTTPHandleFunc(s.handleEndQuiz))
	router.HandleFunc("/quiz/{id}/evaluate", makeHTTPHandleFunc(s.handleEvaluateQuiz))
	router.HandleFunc("/quiz/{id}/answers", makeHTTPHandleFunc(s.handleGetAnswersQuiz))
	router.HandleFunc("/quiz/session", makeHTTPHandleFunc(s.handleGetAllSessionsID))


	router.HandleFunc("/get/language", makeHTTPHandleFunc(s.handleGetAllLanguages))
	router.HandleFunc("/get/language/{lang}", makeHTTPHandleFunc(s.handleGetLanguageByName))


	return router
}
