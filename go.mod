module azla_go_learning

go 1.22.1

require (
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/gorilla/securecookie v1.1.2 // indirect
	github.com/gorilla/sessions v1.2.2 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/markbates/goth v1.79.0 // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/oauth2 v0.17.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.32.0 // indirect
)
