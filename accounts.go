package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"strings"
	"encoding/json"
)

func (s *APIServer) handleAccountSettings(w http.ResponseWriter, r *http.Request) error {
	switch r.Method {
	case "POST":
		return s.handleCreateAccountSettings(w, r)
	case "GET":
		return s.handleGetAccountSettings(w, r)
	default:
		return nil
	}
}

func (s *APIServer) handleCreateAccountSettings(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	settings := new(AccountSettingRequest)
	if err := json.NewDecoder(r.Body).Decode(&settings); err != nil {
		return err
	}

	if settings.Language == "" {
		settings.Language = language
	}

	accountSetting := NewAccountSettings(account.ID, settings.Language)

	_, err = s.store.GetAccountSettingsForAccount(account.ID)
	if err != nil {
		if err = s.store.CreateAccountSettingsForAccount(accountSetting); err != nil {
			return err
		}
	}

	return WriteJSON(w, http.StatusOK, accountSetting)
}

func (s *APIServer) handleGetAccountSettings(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	progress, err := s.store.GetAccountSettingsForAccount(account.ID)
	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, progress)
}

func (s *APIServer) handleUpdateAccountSettings(w http.ResponseWriter, r *http.Request) error {
	attr := mux.Vars(r)["attribute"]

	switch attr {
	case "dark_mode":
		update := mux.Vars(r)["update"]
		updateBool, err := strconv.ParseBool(update)

		account := new(Account)
		if account, err = s.getAccountByID(r); err != nil {
			return err
		}

		accountSettings := updateDarkMode(updateBool)

		if err = s.store.UpdateAccountSettingsDarkMode(account.ID, accountSettings); err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, accountSettings)
	case "new_account":
		update := mux.Vars(r)["update"]
		updateBool, _ := strconv.ParseBool(update)
		
		account, err := s.getAccountByID(r)
		if err != nil {
			return err 
		}

		accountSettings := updateIsNewAccount(updateBool)

		if err := s.store.UpdateAccountSettingsNewAccount(account.ID, accountSettings.IsNewAccount); err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, accountSettings)
	case "language":
		update := mux.Vars(r)["update"]
		
		account, err := s.getAccountByID(r)
		if err != nil {
			return err 
		}

		accountSettings := updateDefaultLanguage(update)

		if err := s.store.UpdateAccountSettingsLanguage(account.ID, accountSettings.DefaultLanguage); err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, accountSettings)

	case "picture":
		account, err := s.getAccountByID(r)
		if err != nil {
			return err 
		}

		createReq := new(UpdatePRPRequest)
		if err := json.NewDecoder(r.Body).Decode(&createReq); err != nil {
			return err
		}

		accountSettings := updateProfilePicture(createReq.ProfilePicture)

		if err := s.store.UpdateAccountSettingsPFP(account.ID, accountSettings); err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, accountSettings)

	default:
		return fmt.Errorf("attribute not supported %s", attr)

	}
}

func (s *APIServer) handleIfAccountHaveLanguages(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	progress, err := s.store.GetAllProgressForAccount(account.ID)
	if err != nil {
		return err
	}

	if len(progress) == 0 {
		s.store.UpdateAccountSettingsNewAccount(account.ID, true)
		return WriteJSON(w, http.StatusNotFound, map[string]bool{"new_account": true})
	} else {
		s.store.UpdateAccountSettingsNewAccount(account.ID, false)
		return WriteJSON(w, http.StatusNotFound, map[string]bool{"new_account": false})
	}

}

func updateDarkMode(darkMode bool) *AccountSettings {
	return &AccountSettings{
		DarkMode: darkMode,
	}
}


func updateDefaultLanguage(language string) *AccountSettings {
	language = strings.ToLower(language)
	return &AccountSettings{
		DefaultLanguage: language,
	}
}

func updateProfilePicture(pfp string) *AccountSettings {
	return &AccountSettings{
		ProfilePicture: pfp,
	}
}


func updateIsNewAccount(status bool) *AccountSettings {
	return &AccountSettings{
		IsNewAccount: status,
	}
}
