package main

import (
	"azla_go_learning/internal/words/aze"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"strings"
)


// default language
var language = "azerbajani"

func (s *APIServer) handleProgress(w http.ResponseWriter, r *http.Request) error {
	switch r.Method {
	case "POST": // create progress on post request
		return s.handleCreateProgress(w, r, "")
	case "GET": // retrive progression
		return s.handleGetProgress(w, r, "")
	case "PUT": // retrive progression
		return s.handleResetProgress(w, r, "")
	case "DELETE": // retrive progression
		return s.handleDeleteProgress(w, r, "")
	default:
		return nil
	}
}

func (s *APIServer) handleCreateProgress(w http.ResponseWriter, r *http.Request, lang string) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	if lang == "" {
		lang = language
	}

	// create progress map to keep track of progress
	newProgressForUser := map[string]interface{}{
		"Section":        1,
		"progress": map[string]map[int]map[int]bool{
			"Unit1": {
				1: {1: true},
			},
		},
	}
	
	lang = strings.ToLower(lang)
	progress := NewProgress(account.UserName, account.ID, newProgressForUser, lang)

	err = checkIfLanguagesIsOk(s, lang)

	if err != nil {
		return err
	}
	
	return checkIfProgressExistForUser(w, s, account, progress, lang)

}


func (s *APIServer) handleResetProgress(w http.ResponseWriter, r *http.Request, lang string) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	if lang == "" {
		lang = language
	}

	//create progress map to keep track of progress
	newProgressForUser := map[string]interface{}{
		"Section":        1,
		"progress": map[string]map[int]map[int]bool{
			"Unit1": {
				1: {1: true},
			},
		},
	}

	progress := NewProgress(account.UserName, account.ID, newProgressForUser, lang)

	if err = s.store.CreateProgressForAccount(progress); err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, progress)

}


func (s *APIServer) handleDeleteProgress(w http.ResponseWriter, r *http.Request, lang string) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	if lang == "" {
		lang = language
	}

	err = s.store.DeleteProgressForAccount(account.ID, lang)
	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, account.ID)

}


func (s *APIServer) handleDeleteAllProgress(w http.ResponseWriter, r *http.Request) error {
	return nil
}


func (s *APIServer) handleResetAllProgress(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	progress, err := s.store.GetAllProgressForAccount(account.ID) 
	finalResult := make(map[string]*Progress)

	for index := range progress {
		// create progress map to keep track of progress
		newProgressForUser := map[string]interface{}{
			"Section":        1,
			"progress": map[string]map[int]map[int]bool{
				"Unit1": {
					1: {1: true},
				},
			},
		}

		resetProgress := resetProgress(newProgressForUser, progress[index].Language)
		language := progress[index].Language

		finalResult[language] = resetProgress

		if err = s.store.UpdateProgressForAccount(account.ID, resetProgress); err != nil {
			return err
		}

	}
	return WriteJSON(w, http.StatusOK, finalResult)
}

func (s *APIServer) handleGetProgress(w http.ResponseWriter, r *http.Request, lang string) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	if lang == "" {
		lang = language
	}

	progress, err := s.store.GetProgressForAccount(account.ID, lang)
	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, progress)
}


func (s *APIServer) handleGetProgressByLanguage(w http.ResponseWriter, r *http.Request) error {
	lang := mux.Vars(r)["lang"]
	switch lang {
	case "aze":
		lang = "azerbajani"
	}
	switch r.Method {
	case "POST":
		return s.handleCreateProgress(w, r, lang)
	case "GET":
		return s.handleGetProgress(w, r, lang)
	case "PUT":
		return s.handleResetProgress(w, r, lang)
	case "DELETE":
		return s.handleDeleteProgress(w, r, lang)
	}

	return nil
}


func (s *APIServer) handleGetAllProgress(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}
	allProgress, err := s.store.GetAllProgressForAccount(account.ID)

	return WriteJSON(w, http.StatusOK, allProgress)

}


// handler is deactivated so you cant update your progress by an api request
func (s *APIServer) handleUpdateProgress(w http.ResponseWriter, r *http.Request) error {
	level := mux.Vars(r)["level"]
	cat := mux.Vars(r)["cat"]

	updateReq := new(UpdateProgressRequest)
	if err := json.NewDecoder(r.Body).Decode(&updateReq); err != nil {
		return err
	}

	err := checkCategoryAndLevel(cat, level)
	if err != nil {
		return err
	}

	levelNum, err := getLevelNumber(level)
	if err != nil {
		return  err
	}

	account := new(Account)
	if account, err = s.getAccountByID(r); err != nil {
		return err
	}

	currentProgress, err := s.store.GetProgressForAccount(account.ID, language)

	_, err = appendProgressList(currentProgress, levelNum, cat)

	if err != nil {
		return err
	}

	progress := updateProgress(map[string]interface{}{})

	if err = s.store.UpdateProgressForAccount(account.ID, progress); err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, progress)
}

func(s *APIServer) appendSkipProgress(account *Account, stage string) {
	newStage := stage
	stageNumberSlice := strings.Split(newStage, "")
	newStageMap := make(map[string]map[int]map[int]bool)
	stageNum := 0

	for _, stage := range stageNumberSlice {
		num,_ := strconv.Atoi(stage)
		stageNum = num
	}

	for i := 1; i <= stageNum; i++ {
		newStageMap["Unit"+strconv.Itoa(i)] = make(map[int]map[int]bool)
		if i < stageNum {
			for j := 1; j <= 5; j++ {
				newStageMap["Unit"+strconv.Itoa(i)][j] = make(map[int]bool)
				for l := 1; l <= 4; l++ {
					newStageMap["Unit"+strconv.Itoa(i)][j][l] = true
				}
			}
		} else {
			newStageMap["Unit"+strconv.Itoa(i)][1] = make(map[int]bool)
			newStageMap["Unit"+strconv.Itoa(i)][1][1] = true
		}
	}

	progress := updateProgress(newStageMap) 

	if err := s.store.UpdateProgressForAccount(account.ID, progress); err != nil {
		return 
	}

}

func appendProgressList(currentProgress *Progress, level int, cat string) ([]map[string]int, error) {
	completedStageStr := currentProgress.Level
	completedStagesStr := fmt.Sprintf("%v", completedStageStr["completedStage"])

	words := strings.Fields(completedStagesStr)
	currentCompleteMap := map[string]int{}
	complete := []map[string]int{}

	for _, stageMap := range words {
		stageMap = strings.ReplaceAll(stageMap, "[", "")
		stageMap = strings.ReplaceAll(stageMap, "]", "")
		stageMap = strings.ReplaceAll(stageMap, "map", "")
		lvl := strings.Split(stageMap, ":")
		lvlToNum, err := strconv.Atoi(lvl[1])
		if err != nil {
			return nil, err

		}
		currentCompleteMap[lvl[0]] = lvlToNum
	}

	completedLevel := level - 1

	_, ok := currentCompleteMap[cat]
	if ok {
		currentCompleteMap[cat] = completedLevel
	} else {
		currentCompleteMap[cat] = completedLevel
	}

	complete = append(complete, currentCompleteMap)

	return complete, nil
}

func appendProgressOnSuccess(currentProgress *Progress, level, stage string, lesson int) map[string]interface{}{
	progressMap := currentProgress.Level["progress"].(map[string]interface{})
	lvlNum := 0
	lvlStr := ""
	for _, lvl := range strings.Split(level, "") { 
		lvlNum,_ = strconv.Atoi(lvl)
		lvlStr = lvl
	}

	levelMap := progressMap[stage].(map[string]interface{})
	lessonMap := levelMap[lvlStr].(map[string]interface{})
	
	lesson += 1
	lessonStr := strconv.Itoa(lesson)
	if lesson != 0 { // make sure lesson is not set to 0 which is not supported
		lessonMap[lessonStr] = true
	}

	if lesson >= 4 { // when you reach the next level (hardcoded)
		lvlNum += 1 
		lvlStr := strconv.Itoa(lvlNum)
		lessonMap, ok := levelMap[lvlStr].(map[string]interface{})
		if !ok {
		    // If the map doesn't exist, initialize it as an empty map
		    lessonMap = make(map[string]interface{})
		    levelMap[lvlStr] = lessonMap
		}
		lessonStr := "1"
		lessonMap[lessonStr] = true
	}
	
	stagesNum := 0
	for _, stageNum := range strings.Split(stage,"") {
		stagesNum, _ = strconv.Atoi(stageNum)
	}

	if lvlNum >= 5 { // when you reach the next stage (hard coded, update if you want to increase number of stages)
		stagesStr := strconv.Itoa(stagesNum + 1)
		levelMap, ok := progressMap["Unit"+stagesStr].(map[string]interface{})
		if !ok {
		    // If the map doesn't exist, initialize it as an empty map
		    levelMap = make(map[string]interface{})
		    lessonMap = make(map[string]interface{})
		    progressMap["Unit"+stagesStr] = levelMap
		}
		lessonStr := "1"
		lessonMap[lessonStr] = true
		levelMap[lvlStr] = lessonMap
	}

	return progressMap

}
func checkIfLanguagesIsOk(s *APIServer, lang string) error{
	_, err := s.store.GetLanguageByName(lang)
	if err != nil {
		return err
	}


	return nil
}

func checkIfProgressExistForUser(w http.ResponseWriter, s *APIServer, 
								account *Account, progress *Progress, 
								lang string) error {
	
	allProgress, err := s.store.GetAllProgressForAccount(account.ID)
	checkIfProgExist := false

	for _, value := range allProgress {
		if value.Language == lang {
			checkIfProgExist = true
		}
	}

	if !checkIfProgExist {
		if err = s.store.CreateProgressForAccount(progress); err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, progress)
	}
	
	return WriteJSON(w, http.StatusOK, progress)

}

func checkCategoryAndLevel(cat, level string) error {
	_, err := aze.GenerateAllWords(cat, level)
	if err != nil {
		return err
	}

	_, err = aze.GenerateMain(cat, level)
	if err != nil {
		return err
	}

	return err
}


func resetProgress(progress interface{}, language string) *Progress {
	return &Progress{
		Language: language,
		Level: map[string]interface{}{
			"section": 1,
			"progress":       progress,
		},
	}
}


func updateProgress(progress interface{}) *Progress {
	return &Progress{
		Language: language,
		Level: map[string]interface{}{
			"section": 1,
			"progress":       progress,
		},
	}
}

