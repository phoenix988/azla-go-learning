package main

import (
	"log"
	"fmt"
	"azla_go_learning/internal/cmd"
)

func seedAccount(s Storage, firstName, lastName, email, password, username string) {
	acc, err := NewAccount(firstName, lastName, email, password, username)
	if err != nil {
		log.Fatal(err)
	}

	if err := s.CreateAccount(acc); err != nil {
		log.Fatal(err)
	}

}

func seedAccounts(s Storage) {
	seedAccount(s, "kalle", "andersson", "kalleandersssson@gmail.com", "hunter", "kalleankaaaaaeeeee")

}

func main() {
	azlaConfig := cmd.ParseFlag()

	store, err := NewPostgresStore()
	if err != nil {
		log.Fatal(err)
	}

	if err := store.Init(store); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%+v\n", store)

	server := NewApiServer(":"+azlaConfig.Port, store)
	server.Run()
}
