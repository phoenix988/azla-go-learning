package main


import (
	"azla_go_learning/internal/words"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)


// Word handlers
func (s *APIServer) handleGetWords(w http.ResponseWriter, r *http.Request) error {
	wordlist := words.GetAllWords()

	return WriteJSON(w, http.StatusOK, wordlist)
}

func (s *APIServer) handleGetWordList(w http.ResponseWriter, r *http.Request) error {
	wordlist := words.GetWordList()
	return WriteJSON(w, http.StatusOK, wordlist)
}

func (s *APIServer) handleUpdateCustomWordList(w http.ResponseWriter, r *http.Request) error {
	updateWordListReq := new(UpdateWordListRequest)
	if err := json.NewDecoder(r.Body).Decode(&updateWordListReq); err != nil {
		return err
	}

	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	checkWordList, err := s.evaluateWordList(updateWordListReq.WordListName, r)
	if err != nil {
		return err
	}

	if checkWordList == false {
		return fmt.Errorf("WordList %s does not exist", updateWordListReq.WordListName)
	}

	newWords := s.createNewWords(updateWordListReq, r)
	_, err = s.store.UpdateWordList(account.Email, updateWordListReq.WordListName, newWords)

	return WriteJSON(w, http.StatusOK, updateWordListReq)
}

func (s *APIServer) handleGetCustomWordList(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	words, err := s.store.GetWordList(account.Email)
	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, words)
}

func (s *APIServer) handleGetWordsByName(w http.ResponseWriter, r *http.Request) error {
	wordlists := words.GetAllWords()
	list := mux.Vars(r)["string"]

	words, err := wordlists[list]

	if err != true {
		return fmt.Errorf("Wordlist does not exist %s", list)
	}

	return WriteJSON(w, http.StatusOK, words)
}

func (s *APIServer) handleGetWordsAmount(w http.ResponseWriter, r *http.Request) error {
	list := mux.Vars(r)["string"]

	wordAmount := words.GetAmountOfWords(list)

	return WriteJSON(w, http.StatusOK, wordAmount)

}

func (s *APIServer) handleCreateWordList(w http.ResponseWriter, r *http.Request) error {
	id, err := getID(r)
	if err != nil {
		return err
	}

	createWordListReq := new(CreateWordListRequest)
	if err := json.NewDecoder(r.Body).Decode(&createWordListReq); err != nil {
		return err
	}

	account, err := s.store.GetAccountByID(id)

	if err != nil {
		return err
	}

	checkWordList, err := s.evaluateWordList(createWordListReq.WordListName, r)
	if err != nil {
		return err
	}

	if checkWordList == true {
		return fmt.Errorf("WordList %s already exist", createWordListReq.WordListName)
	}

	wordList := NewWordList(
		account.Email,
		createWordListReq.WordListName,
		createWordListReq.Words)

	if err := s.store.CreateWordList(wordList); err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, wordList)
}
