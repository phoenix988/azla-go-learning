package main

import (
	"azla_go_learning/internal/char"
	"azla_go_learning/internal/cmd"
	"azla_go_learning/internal/words"
	"github.com/google/uuid"
	"encoding/json"
	"fmt"
	jwt "github.com/golang-jwt/jwt/v4"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type APIServer struct {
	listenAddr string
	store      Storage
}

func NewApiServer(listenAddr string, store Storage) *APIServer {
	return &APIServer{
		listenAddr: listenAddr,
		store:      store,
	}

}

func (s *APIServer) Run() {
	router := mux.NewRouter()


	// cors handlers and middleware
	corsHandler := handlers.CORS(
		handlers.AllowedHeaders([]string{"Content-Type", "Authorization"}),
		handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"}),
		handlers.AllowedOrigins([]string{"*"}),
	)

	// Enable CORS
	corsMiddleware := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
			if r.Method == "OPTIONS" {
				return
			}
			next.ServeHTTP(w, r)
		})
	}
	
	router = s.CreateMainRoutes(router)

	// Attach the middleware
	router.Use(corsMiddleware)

	// Logging
	log.Println("JSON Api server running on port: ", s.listenAddr)

	// Starts the api
	http.ListenAndServe(s.listenAddr, corsHandler(router))
}

func (s *APIServer) handleAccount(w http.ResponseWriter, r *http.Request) error {
	switch r.Method {
	case "GET":
		return s.handleGetAccount(w, r)

	case "POST":
		return s.handleCreateAccount(w, r)

	default:
		return fmt.Errorf("method not allowed %s", r.Method)
	}

}

func (s *APIServer) handleLogin(w http.ResponseWriter, r *http.Request) error {
	if r.Method == "POST" {
		loginReq := new(LoginRequest)
		if err := json.NewDecoder(r.Body).Decode(&loginReq); err != nil {
			return WriteJSON(w, http.StatusForbidden, "login failed")
		}

		// Login using username or email
		account, err := s.store.GetAccountByEmail(loginReq.UserName)
		if err != nil {
			account, err = s.store.GetAccountByUserName(loginReq.UserName)
			if err != nil {
				return WriteJSON(w, http.StatusNotAcceptable, "login failed")
			}
		}

		if !account.ValidPassword(loginReq.Password) {
			return WriteJSON(w, http.StatusForbidden, "login failed")
		}

		token, err := createJWT(account)
		resp := LoginResponse{
			Token: token,
			Email: account.Email,
		}
		if err != nil {
			return err 
		}

		s.handleCreateAccountSettings(w, r)
		return WriteJSON(w, http.StatusOK, resp)

	} else {
		return fmt.Errorf("method not allowed %s", r.Method)
	}
}

func (s *APIServer) handleAccountLogout(w http.ResponseWriter, r *http.Request) error {
	return WriteJSON(w, http.StatusOK, map[string]string{"message": "Logout successful"})
}

func (s *APIServer) handleGetAccount(w http.ResponseWriter, _ *http.Request) error {
	accounts, err := s.store.GetAccounts()

	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, accounts)
}

func (s *APIServer) handleGetAccountByID(w http.ResponseWriter, r *http.Request) error {
	switch r.Method {
	case "GET":
		id, err := getID(r)
		if err != nil {
			return err
		}

		account, err := s.store.GetAccountByID(id)
		if err != nil {
			return err
		}

		account.FirstName = char.FirstLetterToUpper(account.FirstName)
		account.LastName = char.FirstLetterToUpper(account.LastName)

		return WriteJSON(w, http.StatusOK, account)

	case "DELETE":
		return s.handleDeleteAccount(w, r)

	default:
		return fmt.Errorf("method not allowed %s", r.Method)

	}

}

func (s *APIServer) handleGetAccountByEmail(w http.ResponseWriter, r *http.Request) error {
	email := mux.Vars(r)["string"]
	account, err := s.store.GetAccountByEmail(email)
	if err != nil {
		return WriteJSON(w, http.StatusOK, map[string]bool{"emailExist": true})
	}
	account.FirstName = char.FirstLetterToUpper(account.FirstName)
	account.LastName = char.FirstLetterToUpper(account.LastName)

	return WriteJSON(w, http.StatusOK, account)
}

func (s *APIServer) handleGetAccountByUserName(w http.ResponseWriter, r *http.Request) error {
	username := mux.Vars(r)["string"]
	account, err := s.store.GetAccountByUserName(username)
	if err != nil {
		return err
	}
	account.FirstName = char.FirstLetterToUpper(account.FirstName)
	account.LastName = char.FirstLetterToUpper(account.LastName)

	return WriteJSON(w, http.StatusOK, account)
}

func (s *APIServer) handleCreateAccount(w http.ResponseWriter, r *http.Request) error {
	createAccountReq := new(CreateAccountRequest)
	if err := json.NewDecoder(r.Body).Decode(&createAccountReq); err != nil {
		return err
	}

	check, err := s.evaluateCreateAccount(createAccountReq, w)
	if err != nil {
		return err
	}
	
	if check == false {
		return nil
	}

	account, err := NewAccount(createAccountReq.FirstName,
		createAccountReq.LastName,
		createAccountReq.Email,
		createAccountReq.Password,
		createAccountReq.UserName)

	if err != nil {
		return err
	}

	if err := s.store.CreateAccount(account); err != nil {
		return err
	}

	s.handleCreateAccountSettings(w, r)

	return WriteJSON(w, http.StatusOK, createAccountReq)
}

func (s *APIServer) handleUpdateAccountName(w http.ResponseWriter, r *http.Request) error {
	updateAccountReq := new(UpdateNameRequest)
	switch r.Method {
	case "POST":
		id, err := getID(r)

		if err != nil {
			return err
		}

		if err := json.NewDecoder(r.Body).Decode(&updateAccountReq); err != nil {
			return err
		}

		account := UpdateAccountName(updateAccountReq.FirstName, updateAccountReq.LastName)

		if err := s.store.UpdateAccountName(id, account); err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, account)

	default:
		return fmt.Errorf("method not allowed %s", r.Method)

	}
}

func (s *APIServer) handleUpdatePassword(w http.ResponseWriter, r *http.Request) error {
	updatePasswordReq := new(UpdatePasswordRequest)
	switch r.Method {
	case "POST":
		id, err := getID(r)

		if err != nil {
			return err
		}

		if err := json.NewDecoder(r.Body).Decode(&updatePasswordReq); err != nil {
			return fmt.Errorf("Error decoding the response, %s", err)
		}

		account, err := s.store.GetAccountByID(id)
		if err != nil {
			return err
		}

		decodePassword := bcrypt.CompareHashAndPassword([]byte(account.Password), []byte(updatePasswordReq.CurrentPassword))

		if decodePassword != nil {
			return fmt.Errorf("Password does not match")
		}

		checkPass := s.evaluatePassword(updatePasswordReq.Password, updatePasswordReq.ConfirmPassword)
		if checkPass == false {
			return err
		}

		acc := UpdatePassword(updatePasswordReq.Password)

		if err := s.store.UpdatePassword(id, acc); err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, acc)

	default:
		return fmt.Errorf("method not allowed %s", r.Method)

	}
}

func (s *APIServer) handleUpdateEmail(w http.ResponseWriter, r *http.Request) error {
	updateEmailReq := new(UpdateEmailRequest)
	switch r.Method {
	case "POST":
		id, err := getID(r)
		if err != nil {
			return err
		}

		if err := json.NewDecoder(r.Body).Decode(&updateEmailReq); err != nil {
			return fmt.Errorf("Error decoding the response, %s", err)
		}

		checkEmail, err := s.evaluateEmail(updateEmailReq.Email)
		if checkEmail == false {
			return WriteJSON(w, http.StatusNotAcceptable, checkEmail)
		}
		if err != nil {
			return nil
		}

		acc := UpdateEmail(updateEmailReq.Email)

		if err := s.store.UpdateEmail(id, acc); err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, acc)

	default:
		return fmt.Errorf("method not allowed %s", r.Method)

	}
}

func (s *APIServer) handleUpdateUserName(w http.ResponseWriter, r *http.Request) error {
	updateUsernameReq := new(UpdateUsernameRequest)
	switch r.Method {
	case "POST":
		id, err := getID(r)
		if err != nil {
			return err
		}

		if err := json.NewDecoder(r.Body).Decode(&updateUsernameReq); err != nil {
			return fmt.Errorf("Error decoding the response, %s", err)
		}

		checkUserName := s.evaluateUserName(updateUsernameReq.UserName)
		if checkUserName == false {
			return WriteJSON(w, http.StatusNotAcceptable, checkUserName)
		}

		acc := UpdateUserName(updateUsernameReq.UserName)

		if err := s.store.UpdateUserName(id, acc); err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, acc)

	default:
		return fmt.Errorf("method not allowed %s", r.Method)

	}
}

func (s *APIServer) handleUpdateAccount(w http.ResponseWriter, r *http.Request) error {
	action := mux.Vars(r)["action"]
	switch action {
	case "name":
		return s.handleUpdateAccountName(w, r)
	case "password":
		return s.handleUpdatePassword(w, r)
	case "email":
		return s.handleUpdateEmail(w, r)
	case "username":
		return s.handleUpdateUserName(w, r)
	default:
		return fmt.Errorf("action not allowed %s", action)

	}

}

func (s *APIServer) handleDeleteAccount(w http.ResponseWriter, r *http.Request) error {
	id, err := getID(r)
	if err != nil {
		return err
	}

	if err := s.store.DeleteAccount(id); err != nil {
		return err
	}
	return WriteJSON(w, http.StatusOK, map[string]int{"deleted": id})
}

func (s *APIServer) handleGetSessionByID(w http.ResponseWriter, r *http.Request) error {
	session, err := getSession(r)

	if err != nil {
		return err
	}

	switch r.Method {
	case "GET":
		session, err := s.store.GetSessionByID(session)
		if err != nil {
			return err
		}

		err = checkAccessToSession(s, r, w)
		if err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, session)
	case "DELETE":
		return s.handleDeleteSession(w, r)
	default:
		return fmt.Errorf("method not allowed %s", r.Method)

	}

}

func (s *APIServer) handleDeleteSession(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	session, err := getSession(r)

	if err != nil {
		return err
	}

	err = s.store.DeleteSession(account.SessionID)
	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, session)
}

func (s *APIServer) handleIfEmailExist(w http.ResponseWriter, r *http.Request) error {
	email := mux.Vars(r)["email"]
	email = strings.ToLower(email)
	checkEmail,err := s.evaluateEmail(email)

	if err != nil {
		return err
	}

	if checkEmail == false {
		return WriteJSON(w, http.StatusOK, true)
	} else {
		return WriteJSON(w, http.StatusOK, false)
	}
}


func (s *APIServer) handleIfUserExist(w http.ResponseWriter, r *http.Request) error {
	userName := mux.Vars(r)["username"]
	userName = strings.ToLower(userName)
	checkUserName := s.evaluateUserName(userName)

	if checkUserName == false {
		return WriteJSON(w, http.StatusOK, true)
	} else {
		return WriteJSON(w, http.StatusOK, false)
	}
}

// Session Handlers
func (s *APIServer) handleGetSessionAnswers(w http.ResponseWriter, r *http.Request) error {
	session, err := getSession(r)
	if err != nil {
		return err
	}

	sessionAnswers, err := s.store.GetSessionAnswers(session)
	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, sessionAnswers)

}

func (s *APIServer) handleGetAllSessions(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	switch r.Method {
	case "GET":

		sessions, err := s.store.GetAllSessions(account.Email)

		if err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, sessions)

	case "POST":
		return s.handleUpdateActiveSession(w, r)

	default:
		return fmt.Errorf("method not allowed %s", r.Method)

	}
}

func (s *APIServer) handleGetRecentMistakesFromSession(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	switch r.Method {
	case "GET":
		sessions, err := s.store.GetAllSessions(account.Email)
		if err != nil {
			return err
		}

		mistakes := RemoveDuplicate(sessions)

		return WriteJSON(w, http.StatusOK, mistakes)

	default:
		return fmt.Errorf("method not allowed %s", r.Method)

	}

}

func (s *APIServer) handleUpdateActiveSession(w http.ResponseWriter, r *http.Request) error {
	id, err := getID(r)
	if err != nil {
		return err
	}

	updateSessionIDReq := new(UpdateSessionIDRequest)

	if err := json.NewDecoder(r.Body).Decode(&updateSessionIDReq); err != nil {
		return err
	}

	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	allSessions, err := s.store.GetAllSessions(account.Email)
	if err != nil {
		return err
	}

	sessionID, err := s.evaluateSession(id, allSessions, updateSessionIDReq.SessionID)

	if sessionID == nil {
		return fmt.Errorf("session id %d does not exist", updateSessionIDReq.SessionID)
	} else {
		return WriteJSON(w, http.StatusOK, sessionID)
	}

}

func (s *APIServer) handleGetAllSessionsID(w http.ResponseWriter, r *http.Request) error {
	session, err := s.store.GetSessionUniqueID()

	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, session)
}

// Session handlers end

// Quiz related handlers
func (s *APIServer) handleStartQuiz(w http.ResponseWriter, r *http.Request) error {
	list := mux.Vars(r)["string"]
	switch r.Method {
	case "POST": // start the first question
		id, err := getID(r)
		if err != nil {
			return err
		}

		words, err := words.GetWordListByName(list)
		if err != nil {
			return err
		}

		account, err := s.getAccountByID(r)
		if err != nil {
			return err
		}

		question, correct := CreateWordSlice(words)

		identifier, _ := s.GenerateSessionID()

		azlaConfig := cmd.GetFlag()

		maxAmountWordsReq := new(MaxAmountWordRequest)
		err = json.NewDecoder(r.Body).Decode(&maxAmountWordsReq)

		if err != nil {
			maxAmountWordsReq.MaxAmount = azlaConfig.MaxAmount
		}

		session := NewSessionOld(
			account.Email,
			list,
			maxAmountWordsReq.MaxAmount,
			question,
			correct,
			identifier)

		err = s.store.CreateSession(session)
		if err != nil {
			return err
		}

		sessionID := UpdateSessionID(identifier)

		if err := s.store.UpdateSessionID(id, sessionID); err != nil {
			return err
		}

		session.CurrentQuestionImage, err = loadImage(question[0])
		if err != nil {
			return err
		}

		return WriteJSON(w, http.StatusOK, session)
	case "GET":
		return s.handleNextQuiz(w, r)

	default:
		return fmt.Errorf("method not allowed %s", r.Method)

	}
}

func (s *APIServer) handleNextQuiz(w http.ResponseWriter, r *http.Request) error {
	question, err := getQuestion(r)
	if err != nil {
		return err
	}

	list := mux.Vars(r)["string"]
	_, err = words.GetWordListByName(list)
	if err != nil {
		return err
	}

	checkWordList, err := s.evaluateSessionWordList(list, r)

	if err != nil {
		return nil
	} else if checkWordList == false {
		return WriteJSON(w, http.StatusOK, "Wordlist name does not match")
	}

	words, err := s.MoveBetweenQuiz(w, r, question)
	if err != nil {
		return err
	}

	maxAmount, err := strconv.Atoi(words.MaxAmountWords)
	if err != nil {
		return err
	}

	if maxAmount <= question { // last question
		return s.handleEndQuiz(w, r)
	}

	image, err := loadImage(words.Words[question])
	if err != nil {
		return err
	}

	quizSession := &QuizSession{
		CurrentWord:          strings.ReplaceAll(words.Words[question], "{", ""),
		CurrentCorrect:       words.Correct[question],
		CurrentQuestionImage: image,
		CurrentQuestion:      question,
		MaxAmountWords:       maxAmount,
	}

	quizSession.CurrentWord = strings.ReplaceAll(quizSession.CurrentWord, "\"", "")
	quizSession.CurrentWord = strings.ReplaceAll(quizSession.CurrentWord, "}", "")

	return WriteJSON(w, http.StatusOK, quizSession)
}

func (s *APIServer) handleUpdateQuestion(w http.ResponseWriter, r *http.Request) error {
	question, err := getQuestion(r)
	if err != nil {
		return err
	}

	list := mux.Vars(r)["string"]
	_, err = words.GetWordListByName(list)
	if err != nil {
		return err
	}

	account, _ := s.getAccountByID(r)

	quizSession := &QuizSession{
		CurrentQuestion: question,
	}

	s.store.UpdateSessionQuestion(question, account.SessionID) // write to database

	return WriteJSON(w, http.StatusOK, quizSession)
}

func (s *APIServer) handleUpdateAnswer(w http.ResponseWriter, r *http.Request) error {
	question, err := getQuestion(r)
	if err != nil {
		return err
	}

	evaluateAnswerReq := new(EvaluateAnswerRequest)
	if err := json.NewDecoder(r.Body).Decode(&evaluateAnswerReq); err != nil {
		return err
	}

	account, _ := s.getAccountByID(r)
	err = s.UpdateSessionAnswers(account.SessionID, question, evaluateAnswerReq.Answer, r)
	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, evaluateAnswerReq)

}

func (s *APIServer) handleJumpQuiz(w http.ResponseWriter, r *http.Request) error {
	jumpToQuestionReq := new(JumpToQuestionRequest)

	if err := json.NewDecoder(r.Body).Decode(&jumpToQuestionReq); err != nil {
		return err
	}

	words, _ := s.MoveBetweenQuiz(w, r, jumpToQuestionReq.ToQuestion)


	image, err := loadImage(words.Words[jumpToQuestionReq.ToQuestion])
	if err != nil {
		return err
	}

	quizSession := &QuizSession{
		CurrentWord:          words.Words[jumpToQuestionReq.ToQuestion],
		CurrentCorrect:       words.Correct[jumpToQuestionReq.ToQuestion],
		CurrentQuestionImage: image,
		CurrentQuestion:      jumpToQuestionReq.ToQuestion,
	}

	return WriteJSON(w, http.StatusOK, quizSession)
}

func (s *APIServer) handlePrevQuiz(w http.ResponseWriter, r *http.Request) error {
	question, err := getQuestion(r)
	if err != nil {
		return err
	}

	list := mux.Vars(r)["string"]
	_, err = words.GetWordListByName(list)
	if err != nil {
		return err
	}

	words, _ := s.MoveBetweenQuiz(w, r, question)

	if question == 0 {
		return WriteJSON(w, http.StatusOK, false)
	}

	question -= 1

	image, err := loadImage(words.Words[question])
	if err != nil {
		return err
	}

	quizSession := &QuizSession{
		CurrentWord:          words.Words[question],
		CurrentCorrect:       words.Correct[question],
		CurrentQuestionImage: image,
		CurrentQuestion:      question,
	}

	return WriteJSON(w, http.StatusOK, quizSession)
}

func (s *APIServer) handleEndQuiz(w http.ResponseWriter, r *http.Request) error {
	result := new(Result)

	id, err := getID(r)
	if err != nil {
		return err
	}

	account, err := s.store.GetAccountByID(id)
	if err != nil {
		return err
	}

	sessionData, err := s.store.GetSession(account.SessionID)
	if err != nil {
		return err
	}

	maxAmount, _ := strconv.Atoi(sessionData.MaxAmountWords)

	result.CorrectAnswers = sessionData.CorrectAnswers
	result.IncorrectAnswers = sessionData.IncorrectAnswers
	result.MaxAmountWords = maxAmount

	return WriteJSON(w, http.StatusOK, result)
}

func (s *APIServer) handleEvaluateQuiz(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	sessionData, err := s.store.GetSession(account.SessionID)
	maxAmount, _ := strconv.Atoi(sessionData.MaxAmountWords)
	answers := make([]string, maxAmount)
	if err != nil {
		return err
	}
	for index, value := range sessionData.Answers {
		if value != "" && containsMeaningfulContent(value) {
			answers = char.AppendStringAtIndex(answers, value, index)
		}
	}

	for index, value := range answers {
		if sessionData.Correct[index] == value {
			s.UpdateResultsOld(r, "correct")
		} else {
			s.UpdateResultsOld(r, "incorrect")
		}

		if index < maxAmount {
			break
		}

	}

	return WriteJSON(w, http.StatusOK, answers)
}

func (s *APIServer) handleGetAnswersQuiz(w http.ResponseWriter, r *http.Request) error {
	account, err := s.getAccountByID(r)
	sessionData, err := s.store.GetSession(account.SessionID)
	maxAmount, _ := strconv.Atoi(sessionData.MaxAmountWords)
	answers := make([]string, maxAmount)
	if err != nil {
		return err
	}
	for index, value := range sessionData.Answers {
		if value != "" && containsMeaningfulContent(value) {
			answers = char.AppendStringAtIndex(answers, value, index)
		}
	}

	return WriteJSON(w, http.StatusOK, answers)
}

func (s *APIServer) handleUpdateSessionIsComplete(w http.ResponseWriter, r *http.Request) error {
	sessionId, err := getSession(r)
	if err != nil {
		return err
	}

	session, err := s.store.GetSessionByID(sessionId)
	if err != nil {
		return err
	}

	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}

	if session.Email != account.Email {
		return fmt.Errorf("access denied")
	}

	err = s.store.UpdateSessionIsComplete(sessionId, true)

	session, err = s.store.GetSessionByID(sessionId)
	if err != nil {
		return err
	}
	
	return WriteJSON(w, http.StatusOK, session)
}


func (s *APIServer) handleGetImage(w http.ResponseWriter, r *http.Request) error {
	word := mux.Vars(r)["word"]

	image, err := loadImage(word)
	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, image)
}


func (s *APIServer) handleGetLanguageByName(w http.ResponseWriter, r *http.Request) error {
	lang := mux.Vars(r)["lang"]
	language, err := s.store.GetLanguageByName(lang)

	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, language)

}

func (s *APIServer) handleGetAllLanguages(w http.ResponseWriter, r *http.Request) error {
	languages, err := s.store.GetLanguages()

	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, languages)

}


func WriteJSON(w http.ResponseWriter, status int, v any) error {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)

	return json.NewEncoder(w).Encode(v)
}

func permissionDenied(w http.ResponseWriter) {
	WriteJSON(w, http.StatusForbidden, ApiError{Error: "permission denied"})
}

func createJWT(account *Account) (string, error) {
	claims := &jwt.MapClaims{
		"expiresAt":    15000,
		"accountEmail": account.Email,
		"accountId": account.ID,
	}

	secret := os.Getenv("JWT_SECRET")
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(secret))
}

func createJWTSession(session *SaveSession) (string, error) {
	claims := &jwt.MapClaims{
		"expiresAt":    15000,
		"sessionId": session.UniqueID,
	}

	secret := os.Getenv("JWT_SECRET")
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(secret))
}

func jwtAuth(w http.ResponseWriter, r *http.Request, account *Account)  error {
	tokenString := r.Header.Get("Authorization")
	token, err := validateJWT(tokenString)

	if err != nil {
		permissionDenied(w)
		return fmt.Errorf("permission denied")
	}

	if !token.Valid {
		permissionDenied(w)
		return fmt.Errorf("permission denied")
	}

	claims := token.Claims.(jwt.MapClaims)
	id, ok := claims["accountId"].(float64)

	if !ok {
		permissionDenied(w)
		return fmt.Errorf("permission denied")
	}

	if account.ID != int(id) {
		permissionDenied(w)
		return fmt.Errorf("permission denied")
	}
	

	return nil
}

func withJWTAuth(handlerFunc http.HandlerFunc, s Storage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//account, err := store.GetUserByJWTToken(token)
		userID, err := getID(r)
		if err != nil {
			permissionDenied(w)
			return
		}
		
		account, err := s.GetAccountByID(userID)
		if err != nil {
			permissionDenied(w)
			return
		}

		err = jwtAuth(w, r, account)
		
		if err == nil {
			handlerFunc(w, r)
		}
	}
}


func withJWTAuthSessionId(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization-Session")
		token, err := validateJWT(tokenString)

		if err != nil {
			permissionDenied(w)
			return 		
		}

		if !token.Valid {
			permissionDenied(w)
			return 		
		}

		sessionId, err := getSession(r)
		if err != nil {
			permissionDenied(w)
			return
		}
		
		claims := token.Claims.(jwt.MapClaims)
		if sessionId != claims["sessionId"]{
			permissionDenied(w)
			return 		
		}

		handlerFunc(w, r)
	}
}


func withJWTAuthEmail(handlerFunc http.HandlerFunc, s Storage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")
		token, err := validateJWT(tokenString)

		if err != nil {
			permissionDenied(w)
			return
		}

		if !token.Valid {
			permissionDenied(w)
			return
		}

		//account, err := store.GetUserByJWTToken(token)
		email := mux.Vars(r)["string"]
		
		account, err := s.GetAccountByEmail(email)
		if err != nil {
			account, err = s.GetAccountByUserName(email)
			if err != nil {
				permissionDenied(w)
				return 
			}

		}

		claims := token.Claims.(jwt.MapClaims)
		if account.Email != claims["accountEmail"] {
			permissionDenied(w)
			return
		}

		handlerFunc(w, r)
	}
}

func withJWTAuthSession(handlerFunc http.HandlerFunc, s Storage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")
		token, err := validateJWT(tokenString)

		if err != nil {
			permissionDenied(w)
			return
		}

		if !token.Valid {
			permissionDenied(w)
			return
		}

		//account, err := store.GetUserByJWTToken(token)
		sessionId, err := getSession(r)
		if err != nil {
			permissionDenied(w)
			return
		}
		
		session, err := s.GetSessionByID(sessionId)
		if err != nil {
			permissionDenied(w)
			return 
		}

		claims := token.Claims.(jwt.MapClaims)
		if session.Email != claims["accountEmail"] {
			permissionDenied(w)
			return
		}

		handlerFunc(w, r)
	}
}

func validateJWT(tokenString string) (*jwt.Token, error) {
	secret := os.Getenv("JWT_SECRET")
	return jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(secret), nil

	})

}

type apiFunc func(http.ResponseWriter, *http.Request) error

type ApiError struct {
	Error string `json:"error"`
}

func CreateWordSlice(words map[string]string) ([]string, []string) {
	wordlist := []string{}
	correctlist := []string{}

	for word, correct := range words {
		wordlist = append(wordlist, word)
		correctlist = append(correctlist, correct)
	}

	return wordlist, correctlist
}

func (s *APIServer) createNewWords(updateWordListReq *UpdateWordListRequest, r *http.Request) map[string]string {
	account, _ := s.getAccountByID(r)

	words, _ := s.store.GetWordList(account.Email)
	newWords := map[string]string{}
	for _, wordList := range words {
		if wordList.WordListName == updateWordListReq.WordListName {
			for word, count := range updateWordListReq.Words {
				wordList.Words[word] = count
			}
			newWords = wordList.Words
		}
	}

	return newWords

}

func makeHTTPHandleFunc(f apiFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := f(w, r); err != nil {
			WriteJSON(w, http.StatusBadRequest, ApiError{Error: err.Error()})
		}
	}
}

func getID(r *http.Request) (int, error) {
	idStr := mux.Vars(r)["id"]
	id, err := strconv.Atoi(idStr)

	if err != nil {
		return id, fmt.Errorf("invalid id given %s", idStr)
	}

	return id, nil
}

func getQuestionNumber(r *http.Request) (int, error) {
	idStr := mux.Vars(r)["question"]
	id, err := strconv.Atoi(idStr)

	if err != nil {
		return id, fmt.Errorf("invalid id given %s", idStr)
	}

	return id, nil
}

func getSession(r *http.Request) (int, error) {
	idStr := mux.Vars(r)["session"]
	id, err := strconv.Atoi(idStr)

	if err != nil {
		return id, fmt.Errorf("invalid session given %s", idStr)
	}

	return id, nil
}

func getQuestion(r *http.Request) (int, error) {
	idStr := mux.Vars(r)["question"]
	id, err := strconv.Atoi(idStr)

	if err != nil {
		return id, fmt.Errorf("invalid question given %s", idStr)
	}

	return id, nil
}

func (s *APIServer) getAccountByID(r *http.Request) (*Account, error) {
	id, err := getID(r)
	if err != nil {
		return nil, err
	}

	account, err := s.store.GetAccountByID(id)
	if err != nil {
		return nil, err
	}

	return account, nil

}

func (s *APIServer) UpdateResultsOld(r *http.Request, calc string) error {
	id, err := getID(r)
	if err != nil {
		return err
	}

	account, err := s.store.GetAccountByID(id)
	if err != nil {
		return err
	}

	words, err := s.store.GetSession(account.SessionID)
	if err != nil {
		return err
	}

	if calc == "correct" {
		words.CorrectAnswers += 1
		err := s.store.UpdateSessionResult(account.SessionID, words.CorrectAnswers, "correct")
		return err

	} else if calc == "incorrect" {
		words.IncorrectAnswers += 1
		err := s.store.UpdateSessionResult(account.SessionID, words.IncorrectAnswers, "incorrect")
		return err
	}

	return err

}

func (s *APIServer) GenerateSessionID() (int, error) {
	_ = uuid.New().String()
	for {
		// Generate a random identifier
		identifier := rand.Intn(100000000)

		// Fetch all existing IDs from the database
		allIDs, err := s.store.GetSessionUniqueID()
		if err != nil {
			return 0, err
		}

		// Check if the generated identifier is already in the list of existing IDs
		unique := true
		for _, id := range allIDs {
			if id == identifier {
				unique = false
				break
			}
		}

		// If the identifier is unique, return it
		if unique {
			return identifier, nil
		}

		// If the identifier is not unique, the loop continues and generates a new one
	}
}

func (s *APIServer) MoveBetweenQuiz(w http.ResponseWriter, r *http.Request, question int) (*SaveSession, error) {
	list := mux.Vars(r)["string"]
	_, err := words.GetWordListByName(list)
	if err != nil {
		return nil, err
	}

	id, err := getID(r)
	if err != nil {
		return nil, err
	}

	account, err := s.store.GetAccountByID(id)
	if err != nil {
		return nil, err
	}

	words, err := s.store.GetSession(account.SessionID)
	if err != nil {
		return nil, err
	}

	return words, nil
}

func (s *APIServer) UpdateSessionAnswers(id, index int, answer string, r *http.Request) error {
	account, _ := s.getAccountByID(r)
	session, _ := s.store.GetSession(account.SessionID)
	maxAmount, _ := strconv.Atoi(session.MaxAmountWords)
	question, err := getQuestion(r)
	if err != nil {
		return err
	}

	err = s.store.UpdateSessionAnswers(
		account.SessionID,
		maxAmount,
		question,
		session.Answers,
		answer)

	if err != nil {
		return err
	}

	return nil
}


func RemoveDuplicate(sessions []*SaveSession) map[int][]string{
		mistakes := map[int][]string{}
		mistakesMap := map[string]int{}

		for _, ses := range sessions {
			mistakes[ses.ID] = []string{}
			for i := 1; i <= len(ses.Sentences); i++ {
				num, ok := ses.Sentences[i]["success"].(float64)
				if ok {
					if int(num) == 0 {
						if mistakesMap[ses.Sentences[i]["question"].(string)] != 1 {
							mistakes[ses.ID] = append(mistakes[ses.ID], ses.Sentences[i]["question"].(string))
						}

						mistakesMap[ses.Sentences[i]["question"].(string)] = 1

					}
				}
			}
		}
	
	return mistakes
}

func checkAccessToSession(s *APIServer, r *http.Request, w http.ResponseWriter) error{
	sessionId, err := getSession(r)
	if err != nil {
		return err
	}

	account, err := s.getAccountByID(r)
	if err != nil {
		return err
	}
	
	session, err := s.store.GetSessionByID(sessionId)
	if err != nil {
		return err
	}

	if session.Email != account.Email {
		return fmt.Errorf("permission denied")
	}

	return nil
}
