package main

import (
	"azla_go_learning/internal/cmd"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/lib/pq"
	"strings"
	"log"
	"math"
)

type Storage interface {
	// Related account management
	CreateAccount(*Account) error
	DeleteAccount(int) error
	GetAccountByID(int) (*Account, error)
	GetAccountByEmail(string) (*Account, error)
	GetAccountByUserName(string) (*Account, error)
	GetAccounts() ([]*Account, error)
	UpdateAccount(*Account) error
	UpdateSessionID(int, *Account) error
	UpdateAccountName(int, *Account) error
	UpdatePassword(int, *Account) error
	UpdateUserName(int, *Account) error
	UpdateEmail(int, *Account) error
	
	// Related to session storage
	CreateSession(*SaveSession) error
	DeleteSession(int) error
	GetSession(int) (*SaveSession, error)
	GetAllSessions(string) ([]*SaveSession, error)
	GetSessionByID(int) (*SaveSession, error)
	GetSessionUniqueID() ([]int, error)
	UpdateSessionResult(int, int, string) error
	UpdateSessionQuestion(int, int) error
	UpdateSessionAnswers(int, int, int, []string, string) error
	UpdateSessionSentences(int, *GenerateSentence) error
	UpdateSessionSentencesBatch([]*SaveSession) error
	UpdateSessionIsComplete(int, bool) error
	UpdateSessionHealth(int, int) error
	UpdateSessionMode(int, string) error
	GetSessionAnswers(int) ([]string, error) // Not in use
	NewSessionId() (int, error)
	
	// Related to custom word list ("Gonna deprecate this function")
	CreateWordList(*CustomWordList) error
	GetWordList(string) ([]*CustomWordList, error)
	UpdateWordList(string, string, map[string]string) (*CustomWordList, error)
	
	// Related to the user progress data
	CreateProgressForAccount(*Progress) error
	DeleteProgressForAccount(int, string) error
	GetProgressForAccount(int, string) (*Progress, error)
	GetAllProgressForAccount(int) ([]*Progress, error)
	UpdateProgressForAccount(int, *Progress) (error)

	// Related to the user settings
	CreateAccountSettingsForAccount(*AccountSettings) error
	GetAccountSettingsForAccount(int) (*AccountSettings, error)
	UpdateAccountSettingsDarkMode(int, *AccountSettings) (error)
	UpdateAccountSettingsLanguage(int, string) (error)
	UpdateAccountSettingsNewAccount(int, bool) (error)
	UpdateAccountSettingsPFP(int, *AccountSettings) (error)

	// Languages available to the app
	GetLanguageByName(string) (*lang, error)
	GetLanguages() ([]*lang, error)

    CreateFriendList(*Friends) (error)
    GetFriends(int) (*Friends, error)
}

type PostgresStore struct {
	db *sql.DB
}

type lang struct {
	Id int `json:"id"`
	Language string `json:"language"`
	FlagPath string `json:"flagpath"`
}

func NewPostgresStore() (*PostgresStore, error) {
	azlaConfig := cmd.GetFlag()
	connStr := strings.Join([]string{
	    "postgres://",
	    azlaConfig.DBUser,
	    ":",
	    azlaConfig.DBPassword,
	    "@",
	    azlaConfig.DB,
	    "/azla_go_learning?sslmode=disable",
	}, "")
	
	db, err := sql.Open("postgres", connStr)

	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return &PostgresStore{
		db: db,
	}, nil
}

// Main Init to initalize all the databases
func (s *PostgresStore) Init(store *PostgresStore) error {
	if err := store.InitAccount(); err != nil {
		log.Fatal(err)
	}

	if err := store.InitSession(); err != nil {
		log.Fatal(err)
	}

	if err := store.InitCustomWords(); err != nil {
		log.Fatal(err)
	}

	if err := store.InitProgress(); err != nil {
		log.Fatal(err)
	}

	if err := store.InitAccountSettings(); err != nil {
		log.Fatal(err)
	}

	if err := store.InitSentencesAndWords(); err != nil {
		log.Fatal(err)
	}
	
	if err := store.InitLanguages(); err != nil {
		log.Fatal(err)
	}

	if err := store.InitAvailableLanguages(); err != nil {
		log.Fatal(err)
	}

	if err := store.InitFriends(); err != nil {
		log.Fatal(err)
	}

	return nil
}

// For user data
func (s *PostgresStore) InitAccount() error {
	return s.createAccountTable()
}

// For Sessioon data
func (s *PostgresStore) InitSession() error {
	return s.createSessionTable()
}

// For custom words (gonna delete this soon)
func (s *PostgresStore) InitCustomWords() error {
	return s.createCustomWordsTable()
}

// For user progression
func (s *PostgresStore) InitProgress() error {
	return s.createProgressTable()
}

func (s *PostgresStore) InitAccountSettings() error {
	return s.createAccountSettingTable()
}

func (s *PostgresStore) InitLanguages() error {
	return s.createLanguageTable()
}

func (s *PostgresStore) InitAvailableLanguages() error {
	return s.createAvailableLanguages()
}

func (s *PostgresStore) InitCreateProgression() error {
	return s.createProgression()
}

func (s *PostgresStore) InitSentencesAndWords() error {
	return s.createSentencesAndWordsTable()
}

func (s *PostgresStore) InitFriends() error {
	return s.createFriendsTable()
}

// Creates all the tables if not exist
func (s *PostgresStore) createAccountTable() error {
	query := `create table if not exists account(
		id serial primary key,
		first_name varchar(50),
		last_name varchar(50),
		email varchar(50) UNIQUE,
		password bytea,
		created_at timestamp,
		session_id INTEGER,
		username varchar(50) UNIQUE
	)`

	_, err := s.db.Exec(query)

	return err
}

func (s *PostgresStore) createSessionTable() error {
	query := `create table if not exists sessions(
		id serial primary key,
		email varchar(50), 
		words VARCHAR(100)[],
		correct VARCHAR(100)[],
		unique_id INTEGER UNIQUE,
		correct_answers INTEGER,
		incorrect_answers INTEGER,
		max_amount_words INTEGER,
		answers JSONB,
		current_question INTEGER,
		wordlist VARCHAR(50),
		current_sentences JSONB not null,
		health INTEGER,
		mode varchar(50),
		created_at timestamp,
		is_completed BOOLEAN,
		selected_level JSONB,
		language varchar(50)

	)`

	_, err := s.db.Exec(query)

	return err
}

func (s *PostgresStore) createCustomWordsTable() error {
	query := `create table if not exists custom_words (
		id serial primary key,
		username varchar(50) not null,
		wordlist_name varchar(50) not null,
		words JSONB
	)`

	_, err := s.db.Exec(query)

	return err
}

func (s *PostgresStore) createProgressTable() error {
	queries := []string{ `create table if not exists progress (
		id serial primary key,
		username varchar(50) not null,
		user_id INTEGER,
		level JSONB,
		language varchar(50) 
	)`,

}

for _, query := range queries {
	_, err := s.db.Exec(query)
	if err != nil {
		fmt.Println("Error executing query:", err)
		return err
		// Handle the error accordingly
	}
}

return nil
}

func (s *PostgresStore) createAccountSettingTable() error {
	query := `create table if not exists account_settings (
		id serial primary key,
		user_id INTEGER,
		dark_mode BOOLEAN,
		profile_picture varchar(256),
		default_language varchar(256),
		is_new_account BOOLEAN
		)`

	_, err := s.db.Exec(query)

	return err
}


func (s *PostgresStore) createFriendsTable() error {
	query := `create table if not exists account_friends (
		id serial primary key,
		user_id INTEGER UNIQUE,
		friend_list JSONB,
		friend_requests JSONB,
		blocked_users JSONB
		)`

	_, err := s.db.Exec(query)

	return err
}

func (s *PostgresStore) createLanguageTable() error {
	query := `create table if not exists languages (
		language_id serial primary key,
		name varchar(50),
		flag_path text not null,
		CONSTRAINT unique_name UNIQUE (name)
		)`

	_, err := s.db.Exec(query)

	return err
}

func (s *PostgresStore) createAvailableLanguages() error {
	languages := []string{"azerbajani", "turkish"}
	languages_path := []string{"/assets/aze-flag.svg", "/assets/turk-flag.png"}

	for index, language := range languages {
		_, err := s.GetLanguageByName(language)
		flagPath := languages_path[index]

		if err != nil {
			query := (`
			insert into languages
			(name, flag_path)
			values
			($1, $2)`)

		resp, err := s.db.Query(
			query,
			language, flagPath)

		if err != nil {
			return err
		}

		defer resp.Close()
		}

	}

	return nil
}

func (s *PostgresStore) createProgression() error {
	stages := []string{"stage1", "stage2"}

	stageCount := 0

	for _, stage := range stages {
			query := (`
			insert into stages
			(stage_name)
			values
			($1)`)

			resp, err := s.db.Query(
				query,
				stage)

			if err != nil {
				return err
			}
			
			defer resp.Close()
			stageCount += 1

	}

	return nil
}

func (s *PostgresStore) createSentencesAndWordsTable() error {
	query := `create table if not exists sentences_words (
		id serial primary key,
		language_id INTEGER,
		sentences JSONB,
		section varchar(50),
		unit varchar(50),
		level varchar(50)
		)`

	_, err := s.db.Exec(query)

	return err
}
// End of create table functions


func (s *PostgresStore) GetLanguageByName(language string) (*lang, error) {
	rows, err := s.db.Query("select * from languages where name = $1", language)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		lang, err := scanIntoLanguage(rows)
        if err != nil {
            return nil, err
        }
        return lang, nil // Return the progress directly here

	}

	return nil, fmt.Errorf("language %s not found", language)

}

func (s *PostgresStore) GetLanguages() ([]*lang, error) {
	rows, err := s.db.Query("select * from languages")

	if err != nil {
		return nil, err
	}

	defer rows.Close()


	languages := []*lang{}
	for rows.Next() {
		lang, err := scanIntoLanguage(rows)

		if err != nil {
			return nil, err
		}

		languages = append(languages, lang)
	}

	return languages, nil
	
}

func (s *PostgresStore) CreateAccount(acc *Account) error {
	query := (`
	insert into account
	(first_name, last_name, email, created_at, password, session_id, username)
	values
	($1, $2, $3, $4, $5, $6, $7)`)

	_, err := s.db.Query(
		query,
		acc.FirstName,
		acc.LastName,
		acc.Email,
		acc.CreatedAt,
		acc.Password, 0,
		acc.UserName)

	if err != nil {
		return err
	}

	return nil

}

func (s *PostgresStore) UpdateAccount(acc *Account) error {
	return nil
}

func updateAccountFirstName(s *PostgresStore, id int, acc *Account) error {
	query := (`update account set first_name = $1 where id = $2`)

	_, err := s.db.Query(query, acc.FirstName, id)

	if err != nil {
		return err
	}

	return err
}

func updateAccountLastName(s *PostgresStore, id int, acc *Account) error {
	query := (`update account set last_name = $1 where id = $2`)

	_, err := s.db.Query(query, acc.LastName, id)

	if err != nil {
		return err
	}

	return err
}

func (s *PostgresStore) UpdateAccountName(id int, acc *Account) error {
	if acc.FirstName != "" {
		err := updateAccountFirstName(s, id, acc)
		if err != nil {
			return err
		}

	}

	if acc.LastName != "" {
		err := updateAccountLastName(s, id, acc)

		if err != nil {
			return err
		}
	}

	return nil
}

func (s *PostgresStore) UpdateEmail(id int, acc *Account) error {
	query := (`update account set email = $1 where id = $2`)

	_, err := s.db.Query(query, acc.Email, id)

	if err != nil {
		return err
	}

	return err
}

func (s *PostgresStore) UpdatePassword(id int, acc *Account) error {
	query := (`update account set password = $1 where id = $2`)

	_, err := s.db.Query(query, acc.Password, id)

	if err != nil {
		return err
	}

	return err

}

func (s *PostgresStore) UpdateUserName(id int, acc *Account) error {
	query := (`update account set username = $1 where id = $2`)

	_, err := s.db.Query(query, acc.UserName, id)

	if err != nil {
		return err
	}
	return err
}

func (s *PostgresStore) UpdateSessionID(id int, acc *Account) error {
	query := (`update account set session_id = $1 where id = $2`)

	_, err := s.db.Query(query, acc.SessionID, id)

	if err != nil {
		return err
	}

	return err

}

func (s *PostgresStore) UpdateSessionResult(id, num int, calc string) error {
	if calc == "correct" {
		query := (`update sessions set correct_answers = $1 where unique_id = $2`)

		_, err := s.db.Query(query, num, id)

		return err

	} else if calc == "incorrect" {
		query := (`update sessions set incorrect_answers = $1 where unique_id = $2`)

		_, err := s.db.Query(query, num, id)

		return err

	}

	return nil
}


func (s *PostgresStore) DeleteAccount(id int) error {
	_, err := s.db.Query("delete from account where id = $1", id)

	return err

}

func (s *PostgresStore) GetAccountByID(id int) (*Account, error) {
	rows, err := s.db.Query("select * from account where id = $1", id)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		return scanIntoAccount(rows)
	}

	return nil, fmt.Errorf("account %d not found", id)

}

func (s *PostgresStore) GetAccountByEmail(email string) (*Account, error) {
	email = strings.ToLower(email)
	rows, err := s.db.Query("select * from account where email = $1", email)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		return scanIntoAccount(rows)
	}

	return nil, fmt.Errorf("account %s not found", email)

}

func (s *PostgresStore) GetAccountByUserName(username string) (*Account, error) {
	username = strings.ToLower(username)
	rows, err := s.db.Query("select * from account where username = $1", username)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		return scanIntoAccount(rows)
	}

	return nil, fmt.Errorf("account %s not found", username)

}

func (s *PostgresStore) GetAccounts() ([]*Account, error) {
	rows, err := s.db.Query("select * from account")

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	accounts := []*Account{}
	for rows.Next() {
		account, err := scanIntoAccount(rows)

		if err != nil {
			return nil, err
		}

		accounts = append(accounts, account)
	}

	return accounts, nil
}

func (s *PostgresStore) CreateAccountSettingsForAccount(accSet *AccountSettings) error{
	query := (`
	insert into account_settings
	(user_id, dark_mode, profile_picture, default_language, is_new_account)
	values
	($1, $2, $3, $4, $5)`)

	resp, err := s.db.Query(query,accSet.UserID, accSet.DarkMode, accSet.ProfilePicture, 
	accSet.DefaultLanguage, accSet.IsNewAccount)

	if err != nil {
		return err
	}

	defer resp.Close()

	return nil
}


func (s *PostgresStore) GetAccountSettingsForAccount(userID int) (*AccountSettings, error) {
	rows, err := s.db.Query("select * from account_settings where user_id = $1", userID)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		accountSettings, err := scanIntoAccountSettings(rows)
        if err != nil {
            return nil, err
        }
        return accountSettings, nil // Return the progress directly here

	}

	return nil, fmt.Errorf("settings for account %d not found", userID)
}


func (s *PostgresStore) CreateSession(session *SaveSession) error {
	query := (`
	insert into sessions
	(email, words, correct, 
	unique_id, correct_answers, 
	incorrect_answers, max_amount_words, 
	current_question, wordlist, 
	current_sentences, health, mode,
	created_at, is_completed, selected_level,
	language)
	values
	($1, $2, $3, $4, $5, $6, 
	$7, $8, $9, $10, $11, $12, $13, $14, $15, $16)`)

	wordsArray := pq.Array(session.Words)
	correctArray := pq.Array(session.Correct)
	sentencesJSON, err := json.Marshal(session.Sentences)
	if err != nil {
		return err
	}
	
	selectedLevelJSON, err := json.Marshal(session.SelectedLevel)
	if err != nil {
		return err
	}

	resp, err := s.db.Query(
		query,
		session.Email,
		wordsArray,
		correctArray,
		session.UniqueID,
		session.CorrectAnswers,
		session.IncorrectAnswers,
		session.MaxAmountWords,
		session.CurrentQuestion,
		session.WordList,
		sentencesJSON,
		session.Health,
		session.Mode,
		session.CreatedAt,
		session.IsCompleted,
		selectedLevelJSON,
		session.Language,
	)

	if err != nil {
		return err
	}

	defer resp.Close()

	return nil
}


func (s *PostgresStore) UpdateSessionQuestion(question, id int) error {
	query := (`update sessions set current_question = $1 where unique_id = $2`)

	_, err := s.db.Query(query, question, id)

	return err
}

func (s *PostgresStore) UpdateSessionAnswers(id, index, question int, answers []string, answer string) error {
	query := (`update sessions set answers = $1 where unique_id = $2`)

	newAnswers := make([]string, index)
	// Step 2: Modify the specific value in the array
	if index < 0 || index >= len(answers) {
		//return fmt.Errorf("index out of range")
		copy(newAnswers, answers)
		answers = make([]string, index)
	}

	// set new answer
	newAnswers[question] = answer

	answerJSON, err := json.Marshal(newAnswers)
	if err != nil {
		return err
	}

	_, err = s.db.Query(query, answerJSON, id)
	if err != nil {
		return err
	}

	return nil
}


func (s *PostgresStore) UpdateSessionIsComplete(id int, active bool) error {
	query := (`update sessions set is_completed = $1 where unique_id = $2`)
	
	_, err := s.db.Query(query, active, id)
	if err != nil {
		return err
	}

	return nil
}

func (s *PostgresStore) UpdateSessionSentences(id int, sent *GenerateSentence) error {
	query := (`update sessions set current_sentences = $1 where unique_id = $2`)

	sentencesJSON, err := json.Marshal(sent.Sentences)
	if err != nil {
		return err
	}

	_, err = s.db.Query(query, sentencesJSON, id)
	if err != nil {
		return err
	}

	return nil
}


func (s *PostgresStore) UpdateSessionSentencesBatch(sessions []*SaveSession) error {
	// Start a transaction
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}

	query := `UPDATE sessions SET current_sentences = $1 WHERE unique_id = $2`

	stmt, err := tx.Prepare(query)
	if err != nil {
		tx.Rollback()
		return err
	}
	defer stmt.Close()

	for _, session := range sessions {
		sentencesJSON, err := json.Marshal(session.Sentences)
		if err != nil {
			tx.Rollback()
			return err
		}

		_, err = stmt.Exec(sentencesJSON, session.UniqueID)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	// Commit the transaction
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func (s *PostgresStore) UpdateSessionMode(id int, mode string) error {
	query := (`update sessions set mode = $1 where unique_id = $2`)

	_, err := s.db.Query(query,mode,id)
	if err != nil {
		return err
	}

	return nil
}


func (s *PostgresStore) UpdateSessionHealth(id, newHealth int) error {
	query := (`update sessions set health = $1 where unique_id = $2`)

	_, err := s.db.Query(query, newHealth, id)
	if err != nil {
		return err
	}

	return nil
}

func (s *PostgresStore) GetSessionAnswers(id int) ([]string, error) {
	var answers []string

	// Prepare the query
	query := "SELECT answers FROM sessions where unique_id = $1"

	// Execute the query
	rows, err := s.db.Query(query, id)
	if err != nil {
		return nil, err
	}

	// Iterate over the rows and scan the unique_id values
	for rows.Next() {
		var answer string
		if err := rows.Scan(&answer); err != nil {
			return nil, err
		}

		answers = append(answers, answer)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	defer rows.Close()

	return answers, nil
}

func (s *PostgresStore) CreateProgressForAccount(prog *Progress) error{
	query := (`
	insert into progress
	(username, user_id, level, language)
	values
	($1, $2, $3, $4)`)

	levelJSON, err := json.Marshal(prog.Level)
	if err != nil {
		return err
	}

	resp, err := s.db.Query(query,prog.UserName, prog.UserID, levelJSON, prog.Language)

	if err != nil {
		return err
	}

	defer resp.Close()

	return nil
}


func (s *PostgresStore) DeleteProgressForAccount(id int, lang string) error{
	_, err := s.db.Query("delete from progress where user_id = $1 and language = $2", id, lang)

	if err != nil {
		return err
	}

	return nil
}


func (s *PostgresStore) UpdateProgressForAccount(userID int, progress *Progress) (error) {
	query := (`update progress set level = $1 where user_id = $2 and language = $3`)

	levelJSON, err := json.Marshal(progress.Level)
	if err != nil {
		return err
	}
	
	resp, err := s.db.Query(query, levelJSON, userID, progress.Language)
	if err != nil {
		return err
	}

	defer resp.Close()

	return nil
}


func (s *PostgresStore) GetProgressForAccount(userID int, language string) (*Progress, error) {
	rows, err := s.db.Query("select * from progress where user_id = $1 and language = $2", userID, language)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		progress, err := scanIntoProgress(rows)
        if err != nil {
            return nil, err
        }
        return progress, nil // Return the progress directly here

	}

	return nil, fmt.Errorf("progress for account %d not found or user have not started with lang %s", userID, language)
}


func (s *PostgresStore) GetAllProgressForAccount(userID int) ([]*Progress, error) {
	rows, err := s.db.Query("select * from progress where user_id = $1", userID)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	progress := []*Progress{}
	for rows.Next() {
		prog, err := scanIntoProgress(rows)

		if err != nil {
			return nil, err
		}

		progress = append(progress, prog)
	}

	return progress, nil
}


func (s *PostgresStore) UpdateAccountSettingsPFP(userID int, acc *AccountSettings) (error) {
	query := (`update account_settings set profile_picture = $1 where user_id = $2`)
	
	resp, err := s.db.Query(query, acc.ProfilePicture, userID)
	if err != nil {
		return err
	}

	defer resp.Close()

	return nil
}

func (s *PostgresStore) UpdateAccountSettingsDarkMode(userID int, accSet *AccountSettings) (error) {
	query := (`update account_settings set dark_mode = $1 where user_id = $2`)
	
	resp, err := s.db.Query(query, accSet.DarkMode, userID)
	if err != nil {
		return err
	}

	defer resp.Close()

	return nil
}

func (s *PostgresStore) UpdateAccountSettingsLanguage(userID int, language string) (error) {
	query := (`update account_settings set default_language = $1 where user_id = $2`)
	
	resp, err := s.db.Query(query, language, userID)
	if err != nil {
		return err
	}

	defer resp.Close()

	return nil
}

func (s *PostgresStore) UpdateAccountSettingsNewAccount(userID int, status bool) (error) {
	query := (`update account_settings set is_new_account = $1 where user_id = $2`)
	
	resp, err := s.db.Query(query, status, userID)
	if err != nil {
		return err
	}

	defer resp.Close()

	return nil
}

func (s *PostgresStore) DeleteSession(id int) error {
	_, err := s.db.Query("delete from sessions where unique_id = $1", id)

	return err
}

func (s *PostgresStore) GetSession(id int) (*SaveSession, error) {
	rows, err := s.db.Query("select * from sessions where unique_id = $1", id)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		return scanIntoSessions(rows)
	}

	return nil, fmt.Errorf("session ID %d not found", id)

}

func (s *PostgresStore) GetSessionByID(id int) (*SaveSession, error) {
	rows, err := s.db.Query("select * from sessions where unique_id = $1", id)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		return scanIntoSessions(rows)
	}

	return nil, fmt.Errorf("session %d not found", id)
}

func (s *PostgresStore) GetAllSessions(email string) ([]*SaveSession, error) {
	rows, err := s.db.Query("select * from sessions where email = $1", email)

	if err != nil {
		return nil, err
	}

	sessions := []*SaveSession{}
	for rows.Next() {
		session, err := scanIntoSessions(rows)

		if err != nil {
			return nil, err
		}

		sessions = append(sessions, session)
	}

	return sessions, nil

}

func (s *PostgresStore) GetSessionUniqueID() ([]int, error) {
	var uniqueIDs []int

	// Prepare the query
	query := "SELECT unique_id FROM sessions"

	// Execute the query
	rows, err := s.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Iterate over the rows and scan the unique_id values
	for rows.Next() {
		var uniqueID int
		if err := rows.Scan(&uniqueID); err != nil {
			return nil, err
		}

		uniqueIDs = append(uniqueIDs, uniqueID)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return uniqueIDs, nil
}

func (s *PostgresStore) CreateWordList(wordlist *CustomWordList) error {
	words, err := json.Marshal(wordlist.Words)
	if err != nil {
		return err
	}

	query := (`
	insert into custom_words
	(username, wordlist_name, words)
	values
	($1, $2, $3)`)

	_, err = s.db.Query(
		query,
		wordlist.UserName,
		wordlist.WordListName,
		words)

	if err != nil {
		return err
	}

	return nil

}

func (s *PostgresStore) GetWordList(email string) ([]*CustomWordList, error) {
	rows, err := s.db.Query("select * from custom_words where username = $1", email)

	if err != nil {
		return nil, err
	}

	customWordLists := []*CustomWordList{}
	for rows.Next() {
		wordList, err := scanIntoWords(rows)

		if err != nil {
			return nil, err
		}

		customWordLists = append(customWordLists, wordList)
	}

	return customWordLists, nil

}

func (s *PostgresStore) UpdateWordList(email, name string, words map[string]string) (*CustomWordList, error) {
	wordsJSON, err := json.Marshal(words)
	if err != nil {
		return nil, err
	}

	query := (`
	update custom_words
	set words = $3
	where username = $1 and wordlist_name = $2`)

	resp, err := s.db.Query(
		query,
		email,
		name,
		wordsJSON,
	)

	if err != nil {
		return nil, err
	}

	fmt.Printf("%+v\n", resp)

	return nil, err
}


func (s *PostgresStore) GetFriends(userId int) (*Friends, error) {
	rows, err := s.db.Query("select * from account_friends where user_id = $1", userId)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		return scanIntoFriends(rows)
	}

	return nil, fmt.Errorf("friends for user %d not found", userId)
}

func (s *PostgresStore) CreateFriendList(friends *Friends) (error) {
	requests, err := json.Marshal(friends.FriendRequests)
	if err != nil {
		return err
	}

	blocked, err := json.Marshal(friends.BlockedUsers)
	if err != nil {
		return err
	}

	list, err := json.Marshal(friends.FriendList)
	if err != nil {
		return err
	}
		
	query := (`
	insert into account_friends
	(user_id, friend_list, friend_requests, blocked_users)
	values
	($1, $2, $3, $4)`)

	_, err = s.db.Query(
		query,
		friends.UserId,
		list,
		requests,
		blocked)

	if err != nil {
		return err
	}

	return nil
}


func (s *PostgresStore) NewSessionId() (int, error) {
	var identifier int

    // Get the next value from the sequence
    err := s.db.QueryRow("SELECT nextval('session_unique_id_seq')").Scan(&identifier)
    if err != nil {
        return 0, err
    }

	return identifier, nil

}

func scanIntoAccount(rows *sql.Rows) (*Account, error) {
	account := new(Account)
	err := rows.Scan(
		&account.ID,
		&account.FirstName,
		&account.LastName,
		&account.Email,
		&account.Password,
		&account.CreatedAt,
		&account.SessionID,
		&account.UserName)

	if err != nil {
		return nil, err
	}

	return account, err
}

func scanIntoWords(rows *sql.Rows) (*CustomWordList, error) {
	customWordList := new(CustomWordList)
	var wordsJSON []byte
	err := rows.Scan(
		&customWordList.ID,
		&customWordList.UserName,
		&customWordList.WordListName,
		&wordsJSON,
	)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(wordsJSON, &customWordList.Words); err != nil {
		return nil, err
	}

	return customWordList, err
}

func scanIntoFriends(rows *sql.Rows) (*Friends, error) {
	friends := new(Friends)
	var requestJSON []byte
	var blockedJSON []byte
	var listJSON []byte
	err := rows.Scan(
		&friends.ID,
		&friends.UserId,
		&requestJSON,
		&listJSON,
		&blockedJSON,
	)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(requestJSON, &friends.FriendRequests); err != nil {
		return nil, err
	}

	if err := json.Unmarshal(blockedJSON, &friends.BlockedUsers); err != nil {
		return nil, err
	}

	if err := json.Unmarshal(listJSON, &friends.FriendList); err != nil {
		return nil, err
	}

	return friends, err
}

func scanIntoSessions(rows *sql.Rows) (*SaveSession, error) {
	session := new(SaveSession)
	var answersJSON []byte
	var sentencesJSON []byte
	var selectedLevelJSON []byte
	var words, correct sql.NullString
	noneValue := math.MinInt64
	
	if session.Health == noneValue {
		session.Health = 0
	}

	err := rows.Scan(
		&session.ID,
		&session.Email,
		&words,
		&correct,
		&session.UniqueID,
		&session.CorrectAnswers,
		&session.IncorrectAnswers,
		&session.MaxAmountWords,
		&answersJSON,
		&session.CurrentQuestion,
		&session.WordList,
		&sentencesJSON,
		&session.Health,
		&session.Mode,
		&session.CreatedAt,
		&session.IsCompleted,
		&selectedLevelJSON,
		&session.Language,

		
	)

	if err != nil {
		return nil, err
	}

	if words.Valid {
		session.Words = strings.Split(words.String, ",") // Convert PostgreSQL array string to slice of strings
	}

	if correct.Valid {
		session.Correct = strings.Split(correct.String, ",") // Convert PostgreSQL array string to slice of strings
	}

	if len(answersJSON) == 0 {
		session.Answers = make([]string, 0)
	} else {
		// Unmarshal answersJSON into session.Answers
		if err := json.Unmarshal(answersJSON, &session.Answers); err != nil {
			return nil, err
		}
	}

	if err := json.Unmarshal(sentencesJSON, &session.Sentences); err != nil {
		return nil, err
	}

	if err := json.Unmarshal(selectedLevelJSON, &session.SelectedLevel); err != nil {
		return nil, err
	}

	return session, err
}


func scanIntoProgress(rows *sql.Rows) (*Progress, error) {
	prog := new(Progress)
	var levelJSON []byte
	err := rows.Scan(
		&prog.ID,
		&prog.UserName,
		&prog.UserID,
		&levelJSON,
		&prog.Language,
	)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(levelJSON, &prog.Level); err != nil {
		return nil, err
	}

	return prog, err
}

func scanIntoLanguage(rows *sql.Rows) (*lang, error) {
	language := new(lang)
	err := rows.Scan(
		&language.Id,
		&language.Language,
		&language.FlagPath,
	)

	if err != nil {
		return nil, err
	}

	return language, err
}


func scanIntoAccountSettings(rows *sql.Rows) (*AccountSettings, error) {
	accSettings := new(AccountSettings)
	err := rows.Scan(
		&accSettings.ID,
		&accSettings.UserID,
		&accSettings.DarkMode,
		&accSettings.ProfilePicture,
		&accSettings.DefaultLanguage,
		&accSettings.IsNewAccount,
	)

	if err != nil {
		return nil, err
	}

	return accSettings, err
}



